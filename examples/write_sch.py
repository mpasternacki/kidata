#!/usr/bin/env python3

# pyright: reportMissingImports=false

import math
import sys
from pathlib import Path

from rich.pretty import pprint

import kidata
from kidata.structure import At, KicadSch
from kidata.structure.graphical import Effects
from kidata.structure.kicad_sch import LabelShape

INCH = 25.4
MIL = INCH / 1000

ROWS = 2
COLS = 2
GRID = (INCH, INCH / 2)

SWITCH_LIB_ID = "Switch:SW_Push_45deg"
SWITCH_ANGLE = 90
SWITCH_FOOTPRINT = "keyswitches:Kailh_socket_MX"
DIODE_LIB_ID = "Diode:1N4148WS"
DIODE_ANGLE = 180

env = kidata.KiCadEnv()
sch = env.new(KicadSch)
sch.path = Path(sys.argv[1])

sw_u = sch.lib_symbols.get_or_add(SWITCH_LIB_ID).unit()
sw_offset = (-(sw_u.pin_at("2").rotate(SWITCH_ANGLE))).copy(x=0, angle=None)

diode_u = sch.lib_symbols.get_or_add(DIODE_LIB_ID).unit()
diode_offset = (-(diode_u.pin_at("2").rotate(DIODE_ANGLE))).unangled

col_dx = sw_u.pin_at("1").rotate(SWITCH_ANGLE).x + math.fabs(
    diode_u.pin_at("1").x - diode_u.pin_at("2").x
)

last_col_junction: list[At] = []
for col in range(COLS):
    at = At((col + 1.5) * GRID[0] + col_dx, 2 * GRID[1], 90)
    sch.place_hierarchical_label(at, f"COL{col+1}", LabelShape.OUTPUT)
    last_col_junction.append(at)

for row in range(ROWS):
    row_y = (row + 3) * GRID[1]
    last_row_junction = At(GRID[0], row_y, 180)

    sch.place_hierarchical_label(last_row_junction, f"ROW{row+1}", LabelShape.INPUT)

    for col in range(COLS):
        ref = f"_{col+1}{row+1}"

        switch = sch.place_symbol(
            SWITCH_LIB_ID, sw_offset + At((col + 1.5) * GRID[0], row_y, SWITCH_ANGLE)
        )
        switch.set_reference(ref, at=switch.at.unangled + At(0, -0.2 * INCH, 90), effects=Effects())
        switch.properties.hide("Value")
        switch.properties["Footprint"] = SWITCH_FOOTPRINT

        diode = sch.place_symbol(
            DIODE_LIB_ID, (diode_offset + switch.pin_at("1")).copy(angle=DIODE_ANGLE)
        )
        diode.set_reference(ref)

        new_row_junction = switch.pin_at("2")
        sch.place_wire(last_row_junction, new_row_junction)
        last_row_junction = new_row_junction

        new_col_junction = diode.pin_at("1")
        sch.place_wire(last_col_junction[col], new_col_junction)
        last_col_junction[col] = new_col_junction

sch.save()
pprint(sch)
