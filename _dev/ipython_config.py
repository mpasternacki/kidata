c = get_config()  # noqa

c.InteractiveShellApp.extensions = ["autoreload", "rich"]
c.InteractiveShellApp.exec_lines = [
    "%autoreload 3",
    "%aimport kidata",
    "from pathlib import Path",
    "import typing as t",
    "from conftest import ENV as env",
    "from kidata import *",
    "from kidata.structure import *",
]
