#!/bin/sh
set -e
export CUSTOM_COMPILE_COMMAND="$0"
cd "$(dirname "$0")"
pip-compile \
  --annotate \
  --annotation-style=line \
  --build-isolation \
  --generate-hashes \
  --header \
  --quiet \
  --reuse-hashes \
  "${@}"
mv requirements.txt requirements.txt~
egrep -v '^(-e file:\.\.|# WARNING: pip install will require the following package to be hashed|# Consider using a hashable URL like)' requirements.txt~ > requirements.txt
git --no-pager diff requirements.txt
exec ./pip-sync.sh
