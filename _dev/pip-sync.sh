#!/bin/sh
set -e
cd "$(dirname "$0")"
pip-sync "${@}"
cd ..
flit install --symlink
