#!/bin/sh
set -e

DEV_BASE="$(cd "$(dirname "$0")" ; pwd)"

if ! [ -f "${DEV_BASE}/ipython_config.py" ] ; then
  echo "!!! FATAL: ${DEV_BASE} wrong (ipython_config.py not found)" >&2
  ls -la "${DEV_BASE}"
  exit 1
fi

if ! [ -e "${VIRTUAL_ENV}/bin/activate" ] ; then
  echo "!!! FATAL: must run with active virtualenv" >&2
  exit 1
fi

POSTACTIVATE_PATH="${VIRTUAL_ENV}/bin/postactivate"
IPYTHON_DIR="${VIRTUAL_ENV}/etc/ipython"

if [ -x "${VIRTUAL_ENV}/bin/pip-sync" ] ; then
  echo '# pip-tools installed, not installing' >&2
else
  echo '* Installing pip-tools' >&2
  pip install -U pip pip-tools
fi

if [ -x "${VIRTUAL_ENV}/bin/poe" ] ; then
  echo '# poe installed, not running pip-sync' >&2
else
  echo '* Installing dependencies with pip-sync' >&2
  pip-sync "$(dirname "$0")/requirements.txt"
fi

if [ -d .cache/git/kicad -a -d  .cache/git/kicad-symbols -a -d  .cache/git/kicad-footprints ] ; then
  echo '# KiCad data cache present, not downloading' >&2
else
  echo '* Caching KiCad data' >&2
  poe cache-kicad
fi

echo "* Creating postactivate hook ${POSTACTIVATE_PATH}" >&2
cat > "${POSTACTIVATE_PATH}" <<EOF
#!/usr/bin/env zsh

export KICAD6_DEMO_DIR='$(pwd)/.cache/git/kicad/demos'
export KICAD6_SYMBOL_DIR='$(pwd)/.cache/git/kicad-symbols'
export KICAD6_FOOTPRINT_DIR='$(pwd)/.cache/git/kicad-footprints'

eval "\$(poe _zsh_completion)"
compdef _poe poe
EOF

echo "* Saving IPython config in ${IPYTHON_DIR}/ipython_config.py" >&2
mkdir -pv "${IPYTHON_DIR}"
ln -sfv "${DEV_BASE}/ipython_config.py" "${IPYTHON_DIR}/ipython_config.py"

cat <<EOF >&2
***
*** Virtualenv ${VIRTUAL_ENV} initialized.
*** Reactivate it or source ${POSTACTIVATE_PATH} to make it work.
*** You can safely rerun this script to update settings.
***
EOF
