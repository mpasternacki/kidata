# Development Environment

These are files and script I use to configure my own dev environment. They're
customized for my preferences and not a part of the project. If you intend to
work on KiParse, they might be helpful anyway.
