from __future__ import annotations

import typing as t

from ._util import Self


@t.final
class Bareword:
    From = Self | str

    __slots__ = ("value",)
    value: t.Final[str]

    def __init__(self, value: str):
        self.value = value

    @classmethod
    def from_(cls, arg: From) -> Self:
        if isinstance(arg, cls):
            return arg

        return cls(arg)  # type: ignore[FIXME]

    def __hash__(self) -> int:
        return hash(self.value)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Bareword):
            return self.value == other.value
        if isinstance(other, str):
            return self.value == other
        return NotImplemented

    def __repr__(self) -> str:  # pragma: no cover
        return f"{self.__class__.__qualname__}({self.value!r})"

    def __rich_repr__(self) -> t.Iterator[t.Any]:  # pragma: no cover
        yield self.value

    def __str__(self) -> str:
        return self.value

    def _kiformat_(self, f: t.TextIO) -> None:
        f.write(self.value)
