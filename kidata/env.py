from __future__ import annotations

import os
import platform
import typing as t
from collections import ChainMap
from functools import cached_property
from pathlib import Path
from warnings import warn

if t.TYPE_CHECKING:
    from typing_extensions import Self

import xdg

from ._preloadable import PreloadableChainMap, PreloadableMapping
from .structure import SymLibTable
from .structure.parseable_structure import ParseableStructure
from .structure.structure import Structure
from .structure.symbol import FlatSymbol

_KICAD_VERSION = "6.0"

# https://gitlab.com/kicad/code/kicad/-/blob/master/common/env_vars.cpp


class KiCadDirNotFoundWarning(RuntimeWarning):
    pass


def _warn(message: str) -> None:
    warn(KiCadDirNotFoundWarning(message), stacklevel=2)


_NOT_GIVEN: t.Final = object()

_T_ParseableStructure = t.TypeVar("_T_ParseableStructure", bound=ParseableStructure)
_T_Structure = t.TypeVar("_T_Structure", bound=Structure)


class KiCadEnv(ChainMap[str, str]):
    # TODO: __slots__
    user_config_dir: Path
    user_data_dir: Path
    _system_data_dir: Path

    def __init__(self, *, env: KiCadEnv | None = None, **kwargs: str):
        if env is None:
            super().__init__(kwargs, os.environ)
            match platform.system():
                case "Darwin":
                    self.user_config_dir = (
                        Path.home() / "Library/Preferences/kicad" / _KICAD_VERSION
                    )
                    self.user_data_dir = Path.home() / "Documents/KiCad" / _KICAD_VERSION
                case _:
                    self.user_config_dir = xdg.xdg_config_home() / "kicad" / _KICAD_VERSION
                    self.user_data_dir = xdg.xdg_data_home() / "kicad" / _KICAD_VERSION
            self._system_data_dir = self.__find_kicad6_datadir()  # FIXME: platform-specific stuff
            if not self._system_data_dir.exists():
                _warn(
                    f"KiCad data directory {self._system_data_dir} not found,"
                    " please set $KICAD6_DATA_DIR"
                )

            self.setdefault("KICAD6_SYMBOL_DIR", str(self._system_data_dir / "symbols"))
            self.setdefault("KICAD6_3DMODEL_DIR", str(self._system_data_dir / "3dmodels"))
            self.setdefault("KICAD6_FOOTPRINT_DIR", str(self._system_data_dir / "footprints"))
            self.setdefault("KICAD6_TEMPLATE_DIR", str(self._system_data_dir / "template"))
            self.setdefault("KICAD6_DEMO_DIR", str(self._system_data_dir / "demos"))
            self.setdefault("KICAD_USER_TEMPLATE_DIR", str(self.user_data_dir / "template"))
            self.setdefault("KICAD6_3RD_PARTY", str(self.user_data_dir / "3rdparty"))
        else:
            super().__init__(kwargs, *env.maps)
            self.user_config_dir = env.user_config_dir
            self.user_data_dir = env.user_data_dir
            self._system_data_dir = env._system_data_dir

    def __find_kicad6_datadir(self) -> Path:
        if from_env := self.get("KICAD6_DATADIR"):
            return Path(from_env)
        for candidate in (
            "/usr/share/kicad",
            "/usr/local/share/kicad",
            "/Applications/KiCad/KiCad.app/Contents/SharedSupport",
        ):
            candidate_path = Path(candidate)
            if candidate_path.exists():
                return candidate_path
        return Path("/usr/share/kicad")

    def copy(self) -> Self:
        return self.__class__(**self.maps[0])

    __copy__ = copy

    @property
    def global_sym_lib_table_path(self) -> Path:
        return self.user_config_dir / "sym-lib-table"

    @property
    def global_fp_lib_table_path(self) -> Path:
        return self.user_config_dir / "fp-lib-table"

    def open(
        self, path: Path, *, cls: type[_T_ParseableStructure] | None = None, create: bool = False
    ) -> _T_ParseableStructure:
        if cls is None:
            cls = ParseableStructure.class_for_path(path)  # type: ignore
            assert cls is not None
        rv = cls.from_file(path, create=create)
        rv.env = self
        return rv

    def new(self, cls: type[_T_Structure], *args: t.Any, **kwargs: t.Any) -> _T_Structure:
        rv = cls(*args, **kwargs)
        rv.env = self
        return rv

    @cached_property
    def global_sym_lib_table(self) -> SymLibTable:
        return self.open(self.global_sym_lib_table_path, cls=SymLibTable)

    @cached_property
    def sym_lib_table(self) -> PreloadableMapping[str, FlatSymbol]:
        return self.global_sym_lib_table

    def open_project(self, path: os.PathLike[t.Any]) -> KiCadProject:
        return KiCadProject(path, self)


class KiCadProject(KiCadEnv):
    path: Path

    def __init__(self, path: os.PathLike[t.Any], env: KiCadEnv | None = None, **kwargs: str):
        super().__init__(env=env, **kwargs)
        path = Path(path).resolve(strict=True)
        self.path = path

        assert self.path.is_dir()
        assert self.path_to("kicad_pro").exists()

        self["KIPRJMOD"] = str(self.path)

    def path_to(self, basename: str | Path) -> Path:
        if isinstance(basename, str) and "." not in basename:
            basename = f"{self.path.name}.{basename}"
        return self.path / basename

    def open(
        self,
        path: Path | str,
        *,
        cls: type[_T_ParseableStructure] | None = None,
        create: bool = False,
    ) -> _T_ParseableStructure:
        if isinstance(path, str) or not path.is_absolute():
            path = self.path_to(path)
        return super().open(path, cls=cls, create=create)

    @property
    def local_sym_lib_table_path(self) -> Path:
        return self.path / "sym-lib-table"

    @property
    def local_fp_lib_table_path(self) -> Path:
        return self.path / "fp-lib-table"

    @cached_property
    def local_sym_lib_table(self) -> SymLibTable:
        return self.open(self.local_sym_lib_table_path, cls=SymLibTable)

    @cached_property
    def sym_lib_table(self) -> PreloadableMapping[str, FlatSymbol]:
        return PreloadableChainMap(self.global_sym_lib_table, self.local_sym_lib_table)

    def __repr__(self) -> str:
        return f"{self.__class__.__qualname__}({str(self.path)!r})"

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield None, str(self.path)
