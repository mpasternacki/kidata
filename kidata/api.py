from __future__ import annotations

import typing as t
from enum import Enum


class _Default(Enum):
    DEFAULT = object()

    def __bool__(self) -> bool:
        return False

    def __repr__(self) -> str:
        return self.name

    __str__ = __repr__


DEFAULT = _Default.DEFAULT


class CantHappen(NotImplementedError):
    def __init__(self, *args: t.Any, **kwargs: t.Any):
        if not args and not kwargs:
            super().__init__("CAN'T HAPPEN")
        else:
            super().__init__(*args, **kwargs)
