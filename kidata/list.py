from __future__ import annotations

import sys
import typing as t
from copy import deepcopy

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ._util import extract
from .format import kiformat_list
from .typing import KiValue


class List:
    __slots__ = ("car", "cdr")
    car: t.Final[str]
    cdr: t.Final[list[KiValue]]

    def __init__(self, car: str):
        self.car = sys.intern(car)
        self.cdr = []

    def copy(self) -> Self:
        rv = self.__class__(self.car)
        rv.cdr.extend(self.cdr)
        return rv

    __copy__ = copy

    def __deepcopy__(self, memo: t.Any) -> Self:
        rv = self.__class__(self.car)
        rv.cdr.extend(deepcopy(elt, memo) for elt in self.cdr)
        return rv

    @classmethod
    def extract_from(cls, data: list[KiValue], key: str) -> t.Iterator[Self]:
        key = sys.intern(key)
        return extract(data, lambda elt: elt if isinstance(elt, cls) and elt.car is key else None)

    def _kiformat_(self, f: t.TextIO) -> None:
        kiformat_list(self.car, self.cdr, f)

    def __repr__(self):
        elements = [self.car, *self.cdr[:23]]
        elements_s = ", ".join(repr(i) for i in elements)
        more = ", ..." if len(self.cdr) > 23 else ""
        return f"{self.__class__.__qualname__}({elements_s}{more})"

    def __rich_repr__(self) -> t.Iterator[t.Any]:  # pragma: no cover
        yield None, self.car
        for v in self.cdr:
            yield None, v

    def __lshift__(self, other: KiValue) -> Self:
        self.cdr.append(other)
        return self

    @t.overload
    def __getitem__(self, index: str) -> List:
        ...

    @t.overload
    def __getitem__(self, index: int) -> KiValue:
        ...

    def __getitem__(self, index: str | int) -> KiValue:
        if isinstance(index, int):
            return self.cdr[index]

        car = sys.intern(index)
        for elt in self.cdr:
            if isinstance(elt, List) and elt.car is car:
                return elt

        raise KeyError(index)
