from __future__ import annotations

import os
import re
import typing as t
from collections import deque
from collections.abc import Iterator
from enum import Enum

T = t.TypeVar("T")
U = t.TypeVar("U")


def extract(data: list[T], callback: t.Callable[[T], U | None]) -> Iterator[U]:
    i = 0
    while i < len(data):
        if (value := callback(data[i])) is not None:
            yield value
            del data[i]
        else:
            i += 1


class Const(Enum):
    SENTINEL = object()
    Self = object()

    def __repr__(self) -> str:
        return self.name

    __str__ = __repr__


SENTINEL = Const.SENTINEL

if t.TYPE_CHECKING:
    from typing_extensions import Self as Self
else:

    @t.final
    class Self:
        pass


_EXPAND_VARS_RE = re.compile(r"\$\{\s*(\w+)\s*\}")


def expand_vars(text: str, vars: t.Mapping[str, str] | None = None):
    if vars is None:
        vars = os.environ

    def _sub(m: re.Match[str]) -> str:
        return vars[m.group(1)]

    return _EXPAND_VARS_RE.sub(_sub, text)


class ProxiedMappingMixin(t.Mapping[T, U]):
    __slots__ = ()
    _proxied_map: t.Mapping[T, U]

    def __getitem__(self, k: T) -> U:
        return self._proxied_map[k]

    def __len__(self) -> int:
        return len(self._proxied_map)

    def __iter__(self) -> t.Iterator[T]:
        return iter(self._proxied_map)

    def __contains__(self, o: object) -> bool:
        return o in self._proxied_map


def consume(it: t.Iterable[t.Any]) -> None:
    deque(it, maxlen=0)
