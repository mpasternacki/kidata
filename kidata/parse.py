import re
import sys

import ujson

from .bareword import Bareword
from .list import List
from .typing import KiValue

_RULES = re.compile(
    rb"""
    (?:
        \( \s* (?P<list>   \w+                  )
      |        (?P<rparen> \)                   )
      |        (?P<string> " (?:\\.|[^"\\])* "  )
      |        (?P<num>    -? \d+ (?: \. \d+ )? ) (?=[\s)])
      |        (?P<const>  yes | no             ) (?=[\s)])
      |        (?P<word>   [^"()\s] [^\s)]*     )
    )\s*
    """,
    re.DOTALL | re.ASCII | re.VERBOSE,
)

_CONST: dict[bytes, KiValue] = {
    b"yes": True,
    b"no": False,
}

_TEDIT = sys.intern("tedit")


def kiparse(source: bytes | str) -> list[KiValue]:
    if isinstance(source, str):
        source = source.encode()
    source = source.lstrip()
    res: list[List] = [List("")]
    pos = 0
    try:
        while m := _RULES.match(source, pos):
            if key := m["list"]:
                res.insert(0, List(key.decode()))
            elif m["rparen"]:
                ll = res.pop(0)
                res[0] << ll
            elif i := m["num"]:
                if res[0].car is _TEDIT:
                    # HACK HACK HACK: tedit has a bareword hex value,
                    # which can get misinterpreted as int (or worse,
                    # scientific notation float)
                    res[0] << Bareword(i.decode())
                else:
                    res[0] << ujson.loads(i)
            elif w := m["word"]:
                res[0] << Bareword(w.decode())
            elif w := m["const"]:
                res[0] << _CONST[w]
            elif s := m["string"]:
                res[0] << ujson.loads(s)
            pos = m.end()
        assert pos == len(source), "Could not match before input ended"
    except Exception as exc:
        before = source[max(pos - 100, 0) : pos]
        after = source[pos : pos + 100]
        raise ValueError(
            f"Could not tokenize at {pos} ({before!r} <-HERE-> {after!r}): {exc}"
        ) from exc
    assert len(res) == 1
    return res[0].cdr
