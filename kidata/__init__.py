"""Manipulate KiCad 6 SExpression data files."""

__version__ = "0.0.0"
__protected__ = ("structure",)

# <AUTOGEN_INIT>
from . import structure
from .api import DEFAULT, CantHappen
from .bareword import Bareword
from .env import KiCadDirNotFoundWarning, KiCadEnv, KiCadProject
from .format import KiListMixin, kiformat
from .list import List
from .parse import kiparse
from .typing import KiFormattable, KiValue

__all__ = [
    "Bareword",
    "CantHappen",
    "DEFAULT",
    "KiCadDirNotFoundWarning",
    "KiCadEnv",
    "KiCadProject",
    "KiFormattable",
    "KiListMixin",
    "KiValue",
    "List",
    "kiformat",
    "kiparse",
    "structure",
]
# </AUTOGEN_INIT>
