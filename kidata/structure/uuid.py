from __future__ import annotations

import typing as t
import uuid as std_uuid

from .._util import Self
from ..bareword import Bareword
from ..typing import KiValue
from ._helpers import pop_value
from .structure import Structure


class UUID(Structure, car="uuid"):
    __From = std_uuid.UUID | str | None
    From = Self | __From

    __slots__ = ("uuid",)
    uuid: t.Final[std_uuid.UUID]

    def __init__(self, uuid: __From = None):
        if uuid is None:
            self.uuid = std_uuid.uuid4()
        elif isinstance(uuid, std_uuid.UUID):
            self.uuid = uuid
        else:
            self.uuid = std_uuid.UUID(uuid)

        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self.uuid)

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_(cls, arg: From) -> Self:
        if isinstance(arg, cls):
            return arg
        return cls(arg)  # type: ignore[FIXME]

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        uuid_bw = pop_value(data, Bareword)
        return cls(uuid_bw.value)

    @property
    def cdr(self) -> t.Iterator[KiValue]:
        yield Bareword(str(self.uuid))

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield str(self.uuid)

    def uuid5(self, name: str) -> Self:
        return self.__class__(std_uuid.uuid5(self.uuid, name))

    def __eq__(self, other: t.Any) -> bool:
        if isinstance(other, UUID):
            return self.uuid == other.uuid
        if isinstance(other, std_uuid.UUID):
            return self.uuid == other
        if isinstance(other, str):
            return self.uuid == std_uuid.UUID(other)
        return False
