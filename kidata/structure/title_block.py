from __future__ import annotations

import typing as t

from .._util import Self
from ..list import List
from ..typing import KiValue
from ._helpers import extract_value
from .structure import Structure


@t.final
class TitleBlock(Structure, car="title_block"):
    __slots__ = ("title", "date", "rev", "company", "comment")
    title: t.Final[str | None]
    date: t.Final[str | None]
    rev: t.Final[str | None]
    company: t.Final[str | None]
    comment: t.Final[dict[int, str]]

    def __init__(
        self,
        title: str | None = None,
        *,
        date: str | None = None,
        rev: str | None = None,
        company: str | None = None,
        comment: dict[int, str] | None = None,
    ):
        self.title = title
        self.date = date
        self.rev = rev
        self.company = company
        self.comment = comment or {}
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.title,
            date=self.date,
            rev=self.rev,
            company=self.company,
            comment=dict(self.comment),
        )

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        title = extract_value(data, "title", str)
        date = extract_value(data, "date", str)
        rev = extract_value(data, "rev", str)
        company = extract_value(data, "company", str)
        comment: dict[int, str] = {}
        for comment_l in List.extract_from(data, "comment"):
            (num, text) = comment_l.cdr
            assert isinstance(num, int)
            assert num < 10
            assert num > 0
            assert isinstance(text, str)
            assert num not in comment
            comment[num] = text
        return cls(title, date=date, rev=rev, company=company, comment=comment)

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...]]:
        if self.title is not None:
            yield "title", self.title
        if self.date is not None:
            yield "date", self.date
        if self.rev is not None:
            yield "rev", self.rev
        if self.company is not None:
            yield "company", self.company
        for n in sorted(self.comment.keys()):
            yield "comment", n, self.comment[n]
