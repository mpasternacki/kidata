from __future__ import annotations

import typing as t
from collections import deque
from enum import Enum
from warnings import warn

from .._util import Self, extract
from ..bareword import Bareword
from ..list import List
from ..typing import KiValue
from .api import LeftoverDataError, LeftoverDataWarning

T = t.TypeVar("T")


@t.runtime_checkable
class Unlazificatable(t.Protocol):
    def unlazificate(self) -> None:
        ...


class Extractable(t.Protocol):
    @classmethod
    def extract_from(cls, data: list[KiValue]) -> t.Iterator[Self]:
        ...


def check_leftover_data(context: t.Any, data: t.Sequence[t.Any], *, enforce: bool = True):
    if len(data) > 0:
        if enforce:
            raise LeftoverDataError(context, data)
        warn(LeftoverDataWarning(context, data), stacklevel=2)


def must(value: T | None) -> T:
    if value is None:
        raise ValueError("Cannot be None")
    return value


def only(value: t.Iterable[T] | None, *, discard: bool = False) -> T | None:
    if value is None:
        return None
    vi = iter(value)
    try:
        return next(vi, None)
    finally:
        if extras := tuple(vi):
            if discard:
                warn(LeftoverDataWarning("only", extras), stacklevel=2)
            else:
                raise LeftoverDataError("only", extras)


def extract_bareword_flag(data: list[KiValue], word: str | Bareword) -> bool:
    gen = extract(data, lambda elt: True if isinstance(elt, Bareword) and elt == word else None)
    if rv := next(gen, False):
        deque(gen, maxlen=0)  # itertools recipe "consume"
    return rv


def extract_empty_list_flag(data: list[KiValue], key: str) -> bool:
    gen = List.extract_from(data, key)
    if rv := next(gen, None):
        assert len(rv.cdr) == 0
        deque(gen, maxlen=0)  # itertools recipe "consume"
        return True
    return False


def extract_cdr(data: list[KiValue], key: str) -> list[KiValue] | None:
    if rv := only(List.extract_from(data, key)):
        return rv.cdr
    return None


def extract_coords(data: list[KiValue], key: str) -> tuple[float, float]:
    (x, y) = must(extract_cdr(data, key))
    assert isinstance(x, (int, float))
    assert isinstance(y, (int, float))
    return (x, y)


T_Extractable = t.TypeVar("T_Extractable", bound=Extractable)


def extract_list(
    data: list[KiValue], key: str, cls: type[T_Extractable]
) -> list[T_Extractable] | None:
    subdata = extract_cdr(data, key)
    if subdata is None:
        return None
    rv = list(cls.extract_from(subdata))
    check_leftover_data(key, subdata)
    return rv


T_ValueType = t.TypeVar("T_ValueType", str, bool, int, float, Bareword)


def extract_value(
    data: list[KiValue],
    key: str,
    type_: type[T_ValueType],
) -> T_ValueType | None:
    if (cdr := extract_cdr(data, key)) is not None:
        (rv,) = cdr
        if not isinstance(rv, (float, int) if type_ is float else type_):
            raise ValueError(f"Expected {type_}, got {rv!r}")
        return rv  # type: ignore
    return None


def pop_value(
    data: list[KiValue],
    type_: type[T_ValueType],
    index: int = 0,
) -> T_ValueType:
    rv = data.pop(index)
    if not isinstance(rv, (float, int) if type_ is float else type_):
        raise ValueError(f"Expected {type_}, got {rv!r}")
    return rv  # type: ignore


# _sunder_ properties aren't allowed by Enum, cheat it by putting _kiformat_ in
# a base class
class _BarewordEnumKiFormatMixin:
    __slots__ = ()
    value: str

    def _kiformat_(self, f: t.TextIO) -> None:
        f.write(self.value)


class BarewordEnum(_BarewordEnumKiFormatMixin, Enum):
    __slots__ = ()

    @classmethod
    def extract_from(cls, data: list[KiValue]) -> t.Iterator[Self]:
        values = set(item.value for item in cls)  # TODO: cached attribute?
        return extract(
            data, lambda elt: cls(elt) if isinstance(elt, Bareword) and elt in values else None
        )

    @classmethod
    def extract_attribute(cls, data: list[KiValue], key: str) -> Self | None:
        bw = extract_value(data, key, Bareword)
        if bw is None:
            return None
        return cls(bw)

    def __repr__(self) -> str:
        return f"{self.__class__.__qualname__}.{self.name}"
