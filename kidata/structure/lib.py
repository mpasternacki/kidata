from __future__ import annotations

import os
import typing as t
from pathlib import Path

if t.TYPE_CHECKING:
    from typing_extensions import Self

from .._util import expand_vars
from ..typing import KiValue
from ._container import Contained, Container, T_Item
from ._helpers import extract_value, must
from .parseable_structure import ParseableStructure
from .structure import Structure


@t.final
class Lib(Structure, car="lib"):
    __slots__ = ("name", "type", "uri", "options", "descr")
    name: str
    type: str
    uri: str
    options: str
    descr: str

    def __init__(
        self,
        uri: str,
        *,
        name: str | None = None,
        type: str = "KiCad",
        options: str = "",
        descr: str = "",
    ):
        if name is None:
            (name, _) = os.path.splitext(os.path.basename(uri))
        self.name = name
        self.type = type
        self.uri = uri
        self.options = options
        self.descr = descr
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.uri, name=self.name, type=self.type, options=self.options, descr=self.descr
        )

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        name = must(extract_value(data, "name", str))
        type = must(extract_value(data, "type", str))
        uri = must(extract_value(data, "uri", str))
        options = must(extract_value(data, "options", str))
        descr = must(extract_value(data, "descr", str))
        return cls(name=name, type=type, uri=uri, options=options, descr=descr)

    @property
    def cdr(self) -> t.Iterable[tuple[str, KiValue]]:
        yield "name", self.name
        yield "type", self.type
        yield "uri", self.uri
        yield "options", self.options
        yield "descr", self.descr

    @property
    def path(self) -> Path:
        return Path(expand_vars(self.uri, self.env))


class ContainedLib(ParseableStructure, Contained[str, Lib]):
    Container = Container[str, T_Item, Lib]
    __slots__ = ("__name",)
    __name: str

    @classmethod
    def container__key(cls, item: Self | Lib) -> str:
        if isinstance(item, Lib):
            return item.name
        return item.__name

    @classmethod
    def container__load(cls, item: Lib, container: t.Any) -> Self:
        rv = cls.from_file(item.path)
        rv.env = item.env
        rv.__name = item.name
        return rv

    # TODO: container__check_unloaded?
