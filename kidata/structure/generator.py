from __future__ import annotations

import typing as t

from .._util import Self
from ..bareword import Bareword
from ..typing import KiValue
from .structure import Structure


@t.final
class Generator(Structure, car="generator"):
    __slots__ = ("value",)
    value: t.Final[str]

    def __init__(self, value: str = "kidata"):
        self.value = value
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self.value)

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        bw = data.pop()
        assert isinstance(bw, Bareword)
        return cls(bw.value)

    @property
    def cdr(self) -> t.Iterator[KiValue]:
        yield Bareword(self.value)

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.value
