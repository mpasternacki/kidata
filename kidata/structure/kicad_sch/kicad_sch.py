from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...typing import KiValue
from .._helpers import extract_list, extract_value, must, only, pop_value
from ..at import At
from ..generator import Generator
from ..graphical import Graphical, Pts
from ..paper import Paper
from ..parseable_structure import ParseableStructure
from ..structure import Structure
from ..title_block import TitleBlock
from ..uuid import UUID
from ..version import Version
from .bus_entry import BusEntry
from .junction import Junction
from .label import GlobalLabel, HierarchicalLabel, Label, LabelShape
from .lib_symbols import LibSymbols
from .no_connect import NoConnect
from .sheet import Sheet
from .symbol import Symbol
from .wire import Wire, WireBase

# TODO: toplevel/hierarchical sheet distinction?

_T_Label = t.TypeVar("_T_Label", Label, HierarchicalLabel, GlobalLabel)


@t.final
class KicadSch(ParseableStructure, car="kicad_sch", suffix=".kicad_sch"):
    @t.final
    class SheetInstancePath(Structure, car="path"):
        __slots__ = ("path", "page")
        path: t.Final[str]
        page: t.Final[str]

        def __init__(self, path: str, page: str):
            self.path = path
            self.page = page
            super().__init__()

        def copy(self) -> Self:
            return self.__class__(self.path, self.page)

        __copy__ = copy

        def iter_children(self):
            return ()

        @classmethod
        def from_list_data(cls, data: list[KiValue]) -> Self:
            path = pop_value(data, str)
            page = must(extract_value(data, "page", str))
            return cls(path, page)

        @property
        def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...]]:
            yield self.path
            yield "page", self.page

    @t.final
    class SymbolInstancePath(Structure, car="path"):
        __slots__ = ("path", "reference", "unit", "value", "footprint")
        path: t.Final[str]
        reference: t.Final[str]
        unit: t.Final[int]
        value: t.Final[str]
        footprint: t.Final[str]

        def __init__(
            self,
            path: str,
            *,
            reference: str,
            unit: int = 1,
            value: str,
            footprint: str = "",
        ):
            self.path = path
            self.reference = reference
            self.unit = unit
            self.value = value
            self.footprint = footprint
            super().__init__()

        def copy(
            self,
            *,
            path: str | None = None,
            reference: str | None = None,
            unit: int | None = None,
            value: str | None = None,
            footprint: str | None = None,
        ) -> Self:
            return self.__class__(
                path if path is not None else self.path,
                reference=reference if reference is not None else self.reference,
                unit=unit if unit is not None else self.unit,
                value=value if value is not None else self.value,
                footprint=footprint if footprint is not None else self.footprint,
            )

        __copy__ = copy

        def iter_children(self):
            return ()

        @classmethod
        def from_list_data(cls, data: list[KiValue]) -> Self:
            path = pop_value(data, str)
            reference = must(extract_value(data, "reference", str))
            unit = must(extract_value(data, "unit", int))
            value = must(extract_value(data, "value", str))
            footprint = must(extract_value(data, "footprint", str))
            return cls(path, reference=reference, unit=unit, value=value, footprint=footprint)

        @property
        def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...]]:
            yield self.path
            yield "reference", self.reference
            yield "unit", self.unit
            yield "value", self.value
            yield "footprint", self.footprint

    __slots__ = (
        "version",
        "generator",
        "uuid",
        "paper",
        "title_block",
        "lib_symbols",
        "junctions",
        "no_connect",
        "bus_entry",
        "wire",
        "graphical",
        "labels",
        "global_labels",
        "hierarchical_labels",
        "symbols",
        "sheets",
        "sheet_instances",
        "symbol_instances",
    )
    version: Version
    generator: Generator
    uuid: UUID
    paper: Paper | None
    title_block: TitleBlock | None
    lib_symbols: LibSymbols
    junctions: list[Junction]
    no_connect: list[NoConnect]
    bus_entry: list[BusEntry]
    wire: list[WireBase]
    graphical: list[Graphical]
    labels: list[Label]
    global_labels: list[GlobalLabel]
    hierarchical_labels: list[HierarchicalLabel]
    symbols: list[Symbol]
    sheets: list[Sheet]
    sheet_instances: list[KicadSch.SheetInstancePath]
    symbol_instances: dict[str, KicadSch.SymbolInstancePath]

    def __init__(
        self,
        *,
        version: Version | t.Literal[DEFAULT] = DEFAULT,
        generator: Generator | str | t.Literal[DEFAULT] = DEFAULT,
        uuid: UUID | t.Literal[DEFAULT] = DEFAULT,
        paper: Paper | str | None = Paper.DEFAULT_SIZE,
        title_block: TitleBlock | None = None,
        lib_symbols: LibSymbols | None = None,
        junctions: list[Junction] | None = None,
        no_connect: list[NoConnect] | None = None,
        bus_entry: list[BusEntry] | None = None,
        wire: list[WireBase] | None = None,
        graphical: list[Graphical] | None = None,
        labels: list[Label] | None = None,
        global_labels: list[GlobalLabel] | None = None,
        hierarchical_labels: list[HierarchicalLabel] | None = None,
        symbols: list[Symbol] | None = None,
        sheets: list[Sheet] | None = None,
        sheet_instances: list[KicadSch.SheetInstancePath] | None = None,
        symbol_instances: t.Iterable[KicadSch.SymbolInstancePath] = (),
    ):
        if not generator:
            generator = Generator()
        elif isinstance(generator, str):
            generator = Generator(generator)

        if isinstance(paper, str):
            paper = Paper(paper)

        self.version = version if version is not DEFAULT else Version()
        self.generator = generator if generator is not DEFAULT else Generator()
        self.uuid = uuid if uuid is not DEFAULT else UUID()
        self.paper = paper
        self.title_block = title_block
        self.lib_symbols = lib_symbols or LibSymbols()
        self.junctions = junctions or []
        self.no_connect = no_connect or []
        self.bus_entry = bus_entry or []
        self.wire = wire or []
        self.graphical = graphical or []
        self.labels = labels or []
        self.global_labels = global_labels or []
        self.hierarchical_labels = hierarchical_labels or []
        self.symbols = symbols or []
        self.sheets = sheets or []
        self.sheet_instances = sheet_instances or []
        self.symbol_instances = {si.path: si for si in symbol_instances}
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            version=self.version.copy(),
            generator=self.generator.copy(),
            uuid=self.uuid.copy(),
            paper=self.paper.copy() if self.paper else None,
            title_block=self.title_block.copy() if self.title_block else None,
            lib_symbols=self.lib_symbols.copy(),
            junctions=[junction.copy() for junction in self.junctions],
            no_connect=[no_connect.copy() for no_connect in self.no_connect],
            bus_entry=[bus_entry.copy() for bus_entry in self.bus_entry],
            wire=[wire.copy() for wire in self.wire],
            labels=[label.copy() for label in self.labels],
            global_labels=[global_label.copy() for global_label in self.global_labels],
            hierarchical_labels=[
                hierarchical_label.copy() for hierarchical_label in self.hierarchical_labels
            ],
            symbols=[symbol.copy() for symbol in self.symbols],
            sheets=[sheet.copy() for sheet in self.sheets],
            sheet_instances=[sheet_instance.copy() for sheet_instance in self.sheet_instances],
            symbol_instances=(
                symbol_instance.copy() for symbol_instance in self.symbol_instances.values()
            ),
        )

    __copy__ = copy

    def iter_children(self):
        yield self.version
        yield self.generator
        yield self.uuid
        if self.paper:
            yield self.paper
        if self.title_block:
            yield self.title_block
        yield self.lib_symbols
        yield from self.junctions
        yield from self.no_connect
        yield from self.bus_entry
        yield from self.wire
        yield from self.graphical
        yield from self.labels
        yield from self.global_labels
        yield from self.hierarchical_labels
        yield from self.symbols
        yield from self.sheets
        yield from self.sheet_instances
        yield from self.symbol_instances.values()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (version,) = Version.extract_from(data)
        (generator,) = Generator.extract_from(data)
        (uuid,) = UUID.extract_from(data)
        paper = only(Paper.extract_from(data))
        title_block = only(TitleBlock.extract_from(data))
        (lib_symbols,) = LibSymbols.extract_from(data)
        junctions = list(Junction.extract_from(data))
        no_connect = list(NoConnect.extract_from(data))
        bus_entry = list(BusEntry.extract_from(data))
        wire = list(WireBase.extract_from(data))
        graphical = list(Graphical.extract_from(data))
        labels = list(Label.extract_from(data))
        global_labels = list(GlobalLabel.extract_from(data))
        hierarchical_labels = list(HierarchicalLabel.extract_from(data))
        symbols = list(Symbol.extract_from(data))
        sheets = list(Sheet.extract_from(data))
        sheet_instances = extract_list(data, "sheet_instances", KicadSch.SheetInstancePath)
        symbol_instances = extract_list(data, "symbol_instances", KicadSch.SymbolInstancePath)
        return cls(
            version=version,
            generator=generator,
            uuid=uuid,
            paper=paper,
            title_block=title_block,
            lib_symbols=lib_symbols,
            junctions=junctions,
            no_connect=no_connect,
            bus_entry=bus_entry,
            wire=wire,
            graphical=graphical,
            labels=labels,
            global_labels=global_labels,
            hierarchical_labels=hierarchical_labels,
            symbols=symbols,
            sheets=sheets,
            sheet_instances=sheet_instances,
            symbol_instances=symbol_instances or (),
        )

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        yield self.version
        yield self.generator
        yield self.uuid
        yield self.paper
        if self.title_block:
            yield self.title_block
        yield self.lib_symbols
        yield from self.junctions
        yield from self.no_connect
        yield from self.bus_entry
        yield from self.wire
        yield from self.graphical
        yield from self.labels
        yield from self.global_labels
        yield from self.hierarchical_labels
        yield from self.symbols
        yield from self.sheets
        if self.sheet_instances is not None:
            yield "sheet_instances", *self.sheet_instances
        if self.symbol_instances is not None:
            yield "symbol_instances", *self.symbol_instances.values()

    def unlazificate(self) -> None:
        self.lib_symbols.unlazificate()
        for jn in self.junctions:
            jn.unlazificate()
        for sl in self.symbols:
            sl.unlazificate()
        for ll in self.labels:
            ll.unlazificate()
        for gl in self.global_labels:
            gl.unlazificate()
        for hl in self.hierarchical_labels:
            hl.unlazificate()
        for st in self.sheets:
            st.unlazificate()
        super().unlazificate()

    def uuid5(self, name: str) -> UUID:
        return self.uuid.uuid5(name)

    def place_symbol(
        self,
        lib_id: str,
        at: At,
        unit: int = 1,
        *,
        in_bom: bool | None = None,
        on_board: bool | None = None,
        mirror: Symbol.Mirror | None = None,
        uuid: UUID | None = None,
    ):
        sym = Symbol.place(
            self.lib_symbols.get_or_add(lib_id).unit(unit),
            at,
            in_bom=in_bom,
            on_board=on_board,
            mirror=mirror,
            uuid=uuid,
        )
        sym.parent = self
        self.symbols.append(sym)
        return sym

    def place_nc(self, at: At) -> NoConnect:
        nc = NoConnect(at)
        nc.parent = self
        self.no_connect.append(nc)
        return nc

    def place_label(self, at: At, text: str) -> Label:
        label = Label(text, at, fields_autoplaced=True)
        label.parent = self
        self.labels.append(label)
        return label

    def place_hierarchical_label(self, at: At, text: str, shape: LabelShape) -> HierarchicalLabel:
        label = HierarchicalLabel(text, at, shape=shape, fields_autoplaced=True)
        label.parent = self
        self.hierarchical_labels.append(label)
        return label

    def place_global_label(self, at: At, text: str, shape: LabelShape) -> GlobalLabel:
        # TODO: properties
        label = GlobalLabel(text, at, shape=shape, fields_autoplaced=True)
        label.parent = self
        self.global_labels.append(label)
        return label

    def place_wire(self, start: At, end: At) -> Wire:
        # TODO: support Bus
        wire = Wire(Pts(((start.x, start.y), (end.x, end.y))))
        wire.parent = self
        self.wire.append(wire)
        return wire
