from __future__ import annotations

import typing as t

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ...api import DEFAULT
from ...typing import KiValue
from ..at import At
from ..structure import Structure
from ..uuid import UUID


@t.final
class NoConnect(Structure, car="no_connect"):
    __slots__ = ("at", "uuid")
    at: t.Final[At]
    uuid: t.Final[UUID]

    def __init__(
        self,
        at: At,
        uuid: UUID | t.Literal[DEFAULT] = DEFAULT,
    ):
        self.at = at
        self.uuid = uuid if uuid is not DEFAULT else UUID()
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(at=self.at.copy(), uuid=self.uuid.copy())

    __copy__ = copy

    def iter_children(self):
        yield self.at
        yield self.uuid

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> NoConnect:
        (at,) = At.extract_from(data)
        (uuid,) = UUID.extract_from(data)
        return cls(at, uuid)

    @property
    def cdr(self) -> t.Iterable[KiValue]:
        yield self.at.unangled
        yield self.uuid

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.at
        yield self.uuid
