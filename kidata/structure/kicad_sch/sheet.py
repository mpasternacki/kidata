from __future__ import annotations

import typing as t

from ..._util import Self
from ...typing import KiValue
from .._helpers import BarewordEnum, extract_cdr, extract_coords, must, only, pop_value
from ..at import At
from ..graphical import Color, Effects, Fill, Stroke
from ..property import Property
from ..structure import Structure, check_leftover_data
from ..symbol_properties import SymbolProperties
from ..uuid import UUID


@t.final
class Sheet(Structure, car="sheet"):
    @t.final
    class Pin(Structure, car="pin"):
        class ElectricalType(BarewordEnum):
            INPUT = "input"
            OUTPUT = "output"
            BIDIRECTIONAL = "bidirectional"
            TRI_STATE = "tri_state"
            PASSIVE = "passive"

        __slots__ = (
            "name",
            "electrical_type",
            "at",
            "effects",
            "uuid",
        )
        name: t.Final[str]
        electrical_type: t.Final[ElectricalType]
        at: t.Final[At]
        effects: t.Final[Effects]
        uuid: t.Final[UUID]

        def __init__(
            self,
            name: str,
            electrical_type: ElectricalType,
            *,
            at: At,
            effects: Effects | None = None,
            uuid: UUID | None = None,
        ):
            self.name = name
            self.electrical_type = electrical_type
            self.at = at
            self.effects = effects or Effects()
            self.uuid = uuid or UUID()
            super().__init__()

        def copy(self) -> Self:
            return self.__class__(
                self.name,
                self.electrical_type,
                at=self.at.copy(),
                effects=self.effects.copy(),
                uuid=self.uuid.copy() if self.uuid else None,
            )

        def iter_children(self):
            yield self.at
            yield self.effects
            yield self.uuid

        @classmethod
        def from_list_data(cls, data: list[KiValue]) -> Self:
            name = pop_value(data, str)
            (at,) = At.extract_from(data)
            (effects,) = Effects.extract_from(data)
            (uuid,) = UUID.extract_from(data)
            (electrical_type,) = Sheet.Pin.ElectricalType.extract_from(data)
            return cls(name, electrical_type, at=at, effects=effects, uuid=uuid)

        @property
        def cdr(self) -> t.Iterable[KiValue]:
            yield self.name
            yield self.electrical_type
            yield self.at
            yield self.effects
            yield self.uuid

    __slots__ = (
        "at",
        "size",
        "fields_autoplaced",
        "stroke",
        "fill",
        "uuid",
        "properties",
        "pins",
    )
    at: t.Final[At]
    size: t.Final[tuple[float, float]]
    fields_autoplaced: t.Final[bool]
    stroke: t.Final[Stroke]
    fill: t.Final[Fill | Color]
    uuid: t.Final[UUID]
    properties: t.Final[SymbolProperties]
    pins: t.Final[dict[str, Pin]]

    def __init__(
        self,
        *,
        at: At,
        size: tuple[float, float],
        fields_autoplaced: bool = False,
        stroke: Stroke | None = None,
        # (kicad_sch (sheet)) uses undocumented (fill (color ...)), use that form as default
        fill: Fill | Color | None = None,
        uuid: UUID | None = None,
        properties: t.Iterable[Property] = (),
        pins: t.Iterable[Sheet.Pin] = (),
    ):
        self.at = at
        self.size = size
        self.fields_autoplaced = fields_autoplaced
        self.stroke = stroke or Stroke()
        self.fill = fill or Color.default()
        self.uuid = uuid or UUID()
        self.properties = SymbolProperties(properties, parent=self)
        self.pins = {
            pin.name: pin for pin in pins
        }  # FIXME: does not enforce or check name uniqueness
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            at=self.at.copy(),
            size=self.size,
            fields_autoplaced=self.fields_autoplaced,
            stroke=self.stroke.copy(),
            fill=self.fill.copy(),
            uuid=self.uuid.copy(),
            properties=self.properties.copy_values(),
            pins=(pin.copy() for pin in self.pins.values()),
        )

    __copy__ = copy

    def iter_children(self):
        yield self.at
        yield self.stroke
        yield self.fill
        yield self.uuid
        yield from self.pins.values()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (at,) = At.extract_from(data)
        size = extract_coords(data, "size")
        fields_autoplaced = extract_cdr(data, "fields_autoplaced") is not None
        (stroke,) = Stroke.extract_from(data)

        # Documentation says (fill) is same as elsewhere (fill (type TYPE)); in
        # reality, (kicad_sch (sheet)) has (fill (color ...)). Let's accept
        # both.
        fill_data = must(extract_cdr(data, "fill"))
        colors = only(Color.extract_from(fill_data))
        fill = colors if colors is not None else Fill.from_list_data(fill_data)
        check_leftover_data(f"{cls} -> fill", fill_data)

        (uuid,) = UUID.extract_from(data)
        properties = Property.extract_from(data)
        pins = Sheet.Pin.extract_from(data)
        return cls(
            at=at,
            size=size,
            fields_autoplaced=fields_autoplaced,
            stroke=stroke,
            fill=fill,
            uuid=uuid,
            properties=properties,
            pins=pins,
        )

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...] | None]:
        yield self.at.unangled
        yield "size", *self.size
        if self.fields_autoplaced:
            yield "fields_autoplaced",
        yield self.stroke
        if isinstance(self.fill, Fill):
            yield self.fill
        else:
            yield "fill", self.fill
        yield self.uuid
        yield from self.properties.iter_values()
        yield from self.pins.values()
