from __future__ import annotations

import typing as t

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ...typing import KiValue
from .._helpers import BarewordEnum, extract_cdr, pop_value
from ..at import At
from ..graphical import Effects
from ..property import Property
from ..structure import Structure
from ..symbol_properties import SymbolProperties
from ..uuid import UUID


class LabelShape(BarewordEnum):
    INPUT = "input"
    OUTPUT = "output"
    BIDIRECTIONAL = "bidirectional"
    TRI_STATE = "tri_state"


class LabelBase(Structure):

    __slots__ = ("text", "at", "shape", "fields_autoplaced", "effects", "uuid", "properties")
    text: t.Final[str]
    shape: t.Final[LabelShape | None]
    at: t.Final[At]
    fields_autoplaced: bool
    effects: t.Final[Effects]
    uuid: t.Final[UUID]
    properties: t.Final[SymbolProperties]

    def __init__(
        self,
        text: str,
        at: At,
        *,
        shape: LabelShape | None = None,
        fields_autoplaced: bool = False,
        effects: Effects | None = None,
        uuid: UUID | None = None,
        properties: t.Iterable[Property] = (),
    ):
        self.text = text
        self.shape = shape
        self.at = at
        self.fields_autoplaced = fields_autoplaced
        self.effects = effects or Effects()
        self.uuid = uuid or UUID()
        self.properties = SymbolProperties(properties, parent=self)
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.text,
            at=self.at.copy(),
            fields_autoplaced=self.fields_autoplaced,
            effects=self.effects.copy(),
            uuid=self.uuid.copy(),
            properties=self.properties.copy_values(),
        )

    __copy__ = copy

    def iter_children(self):
        yield self.at
        yield self.effects
        yield self.uuid

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        text = pop_value(data, str)
        shape = LabelShape.extract_attribute(data, "shape")
        (at,) = At.extract_from(data)
        fields_autoplaced = extract_cdr(data, "fields_autoplaced") is not None
        (effects,) = Effects.extract_from(data)
        (uuid,) = UUID.extract_from(data)
        properties = Property.extract_from(data)

        return cls(
            text,
            at,
            shape=shape,
            fields_autoplaced=fields_autoplaced,
            effects=effects,
            uuid=uuid,
            properties=properties,
        )

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        yield self.text
        if self.shape:
            yield "shape", self.shape
        yield self.at.angled
        if self.fields_autoplaced:
            yield "fields_autoplaced",
        yield self.effects
        yield self.uuid
        yield from self.properties.iter_values()


@t.final
class Label(LabelBase, car="label"):
    __slots__ = ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        rv = super().from_list_data(data)
        assert rv.shape is None
        assert len(rv.properties) == 0
        return rv


@t.final
class GlobalLabel(LabelBase, car="global_label"):
    __slots__ = ()


@t.final
class HierarchicalLabel(LabelBase, car="hierarchical_label"):
    __slots__ = ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        rv = super().from_list_data(data)
        assert len(rv.properties) == 0
        return rv
