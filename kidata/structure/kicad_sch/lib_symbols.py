from __future__ import annotations

import typing as t

from ..._util import ProxiedMappingMixin, Self
from ...typing import KiValue
from ..structure import Structure
from ..symbol import FlatSymbol


@t.final
class LibSymbols(Structure, ProxiedMappingMixin[str, FlatSymbol], car="lib_symbols"):
    __slots__ = ("_proxied_map",)
    _proxied_map: t.Final[dict[str, FlatSymbol]]

    def __init__(self, symbols: t.Iterable[FlatSymbol] = ()):
        self._proxied_map = {v.name: v.copy() for v in symbols}
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(v.copy() for v in self._proxied_map.values())

    __copy__ = copy

    def iter_children(self):
        yield from self._proxied_map.values()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        return cls(FlatSymbol.extract_from(data))

    @property
    def cdr(self) -> t.Iterable[KiValue | None]:
        yield from self._proxied_map.values()

    def add(self, symbol: FlatSymbol | str) -> FlatSymbol:
        if isinstance(symbol, str):
            fs = self.env.sym_lib_table[symbol]  # type: ignore[FIXME?]
            symbol = t.cast(FlatSymbol, fs)
        self._proxied_map[symbol.name] = symbol
        symbol.parent = self
        return symbol

    def clear(self) -> None:
        self._proxied_map.clear()

    def get_or_add(self, name: str) -> FlatSymbol:
        try:
            return self[name]
        except KeyError:
            return self.add(name)
