from __future__ import annotations

import typing as t

from ..._util import Self
from ...typing import KiValue
from ..graphical import Pts, Stroke
from ..structure import Structure
from ..uuid import UUID


class WireBase(Structure):
    __slots__ = ("pts", "stroke", "uuid")
    pts: t.Final[Pts]
    stroke: t.Final[Stroke]
    uuid: t.Final[UUID]

    def __init__(
        self,
        pts: Pts,
        *,
        stroke: Stroke | None = None,
        uuid: UUID | None = None,
    ):
        self.pts = pts
        self.stroke = stroke or Stroke()
        self.uuid = uuid or UUID()
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self.pts.copy(), stroke=self.stroke.copy(), uuid=self.uuid.copy())

    __copy__ = copy

    def iter_children(self):
        yield self.pts
        yield self.stroke
        yield self.uuid

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (pts,) = Pts.extract_from(data)
        (stroke,) = Stroke.extract_from(data)
        (uuid,) = UUID.extract_from(data)
        return cls(pts, stroke=stroke, uuid=uuid)

    @property
    def cdr(self) -> t.Iterable[KiValue]:
        yield self.pts
        yield self.stroke
        yield self.uuid

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield None, self.pts
        yield "stroke", self.stroke
        yield "uuid", self.uuid


@t.final
class Wire(WireBase, car="wire"):
    __slots__ = ()


@t.final
class Bus(WireBase, car="bus"):
    __slots__ = ()
