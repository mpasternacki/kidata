from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...typing import KiValue
from .._helpers import extract_value, must, only
from ..at import At
from ..graphical import Color
from ..structure import Structure
from ..uuid import UUID


@t.final
class Junction(Structure, car="junction"):
    __slots__ = ("at", "diameter", "color", "uuid")
    at: t.Final[At]
    diameter: t.Final[float]
    color: t.Final[Color]
    uuid: t.Final[UUID | None]

    def __init__(
        self,
        at: At,
        diameter: float = 0,
        *,
        color: Color | t.Literal[DEFAULT] = DEFAULT,
        uuid: UUID | None = None,
    ):
        self.at = at
        self.diameter = diameter
        self.color = color if color is not DEFAULT else Color.default()
        self.uuid = uuid
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.at.copy(),
            self.diameter,
            color=self.color.copy(),
            uuid=self.uuid.copy() if self.uuid else None,
        )

    __copy__ = copy

    def iter_children(self):
        yield self.at
        yield self.color
        if self.uuid:
            yield self.uuid

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (at,) = At.extract_from(data)
        diameter = must(extract_value(data, "diameter", float))
        (color,) = Color.extract_from(data)
        uuid = only(UUID.extract_from(data))
        return cls(at, diameter, color=color, uuid=uuid)

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[str, KiValue] | None]:
        yield self.at.unangled
        yield "diameter", self.diameter
        yield self.color
        yield self.uuid

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.at
        yield "diameter", self.diameter, 0
        yield "color", self.color
        yield "uuid", self.uuid, None
