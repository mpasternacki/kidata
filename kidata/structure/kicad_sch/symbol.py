from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...list import List
from ...typing import KiValue
from .._helpers import BarewordEnum, extract_cdr, extract_value, must, pop_value
from ..at import At
from ..graphical import Effects
from ..property import Property
from ..structure import Structure
from ..symbol import UnitData
from ..symbol_properties import SymbolProperties
from ..uuid import UUID


@t.final
class Symbol(Structure, car="symbol"):
    class Mirror(BarewordEnum):
        __slots__ = ()
        X = "x"
        Y = "y"

    __slots__ = (
        "lib_id",
        "at",
        "mirror",
        "unit",
        "convert",
        "in_bom",
        "on_board",
        "fields_autoplaced",
        "uuid",
        "properties",
        "pins",
        "__unit_data",
    )
    lib_id: str
    at: At
    mirror: Mirror | None
    unit: int
    convert: int | None
    in_bom: bool
    on_board: bool
    fields_autoplaced: bool
    uuid: UUID
    properties: SymbolProperties
    pins: dict[str, UUID]
    __unit_data: UnitData | None

    def __init__(
        self,
        lib_id: str,
        at: At,
        *,
        mirror: Mirror | None = None,
        unit: int = 1,
        convert: int | None = None,
        in_bom: bool = True,
        on_board: bool = True,
        fields_autoplaced: bool = False,
        uuid: UUID | None = None,
        properties: t.Iterable[Property] = (),
        pins: t.Iterable[tuple[str, UUID]] | t.Mapping[str, UUID] = (),
        unit_data: UnitData | None = None,
    ):
        self.lib_id = lib_id
        self.at = at
        self.mirror = mirror
        self.unit = unit
        self.convert = convert
        self.in_bom = in_bom
        self.on_board = on_board
        self.fields_autoplaced = fields_autoplaced
        self.uuid = uuid or UUID()
        self.properties = SymbolProperties(properties, parent=self)
        self.pins = dict(pins)
        self.__unit_data = unit_data
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.lib_id,
            at=self.at.copy(),
            mirror=self.mirror,
            unit=self.unit,
            convert=self.convert,
            in_bom=self.in_bom,
            on_board=self.on_board,
            fields_autoplaced=self.fields_autoplaced,
            uuid=self.uuid.copy(),
            properties=self.properties.copy_values(),
            pins=self.pins.items(),
        )

    __copy__ = copy

    def iter_children(self):
        yield self.at
        yield self.uuid

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        lib_id = must(extract_value(data, "lib_id", str))
        (at,) = At.extract_from(data)
        unit = extract_value(data, "unit", int)
        if unit is None:
            unit = -1
        convert = extract_value(data, "convert", int)
        in_bom = must(extract_value(data, "in_bom", bool))
        on_board = must(extract_value(data, "on_board", bool))
        fields_autoplaced = extract_cdr(data, "fields_autoplaced") is not None
        (uuid,) = UUID.extract_from(data)
        properties = Property.extract_from(data)
        mirror = Symbol.Mirror.extract_attribute(data, "mirror")

        def _gen_pins() -> t.Iterator[tuple[str, UUID]]:
            for pin_l in List.extract_from(data, "pin"):
                pin_cdr = pin_l.cdr
                name = pop_value(pin_cdr, str)
                (uuid,) = UUID.extract_from(pin_cdr)
                assert len(pin_cdr) == 0, f"Leftover pin data of {lib_id} {uuid}: {pin_cdr!r}"
                yield (name, uuid)

        return cls(
            lib_id,
            at,
            mirror=mirror,
            unit=unit,
            convert=convert,
            in_bom=in_bom,
            on_board=on_board,
            fields_autoplaced=fields_autoplaced,
            uuid=uuid,
            properties=properties,
            pins=_gen_pins(),
        )

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        yield "lib_id", self.lib_id
        yield self.at.angled
        if self.mirror:
            yield "mirror", self.mirror
        if self.unit >= 0:
            yield "unit", self.unit
        if self.convert is not None:
            yield "convert", self.convert
        yield "in_bom", self.in_bom
        yield "on_board", self.on_board
        if self.fields_autoplaced:
            yield "fields_autoplaced",
        yield self.uuid
        yield from self.properties.iter_values()
        for num, uuid in self.pins.items():
            yield "pin", num, uuid

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield "lib_id", self.lib_id
        yield "at", self.at
        yield "mirror", self.mirror, None
        yield "unit", self.unit
        yield "convert", self.convert, None
        yield "in_bom", self.in_bom
        yield "on_board", self.on_board
        yield "fields_autoplaced", self.fields_autoplaced, False
        yield "uuid", self.uuid
        yield "properties", self.properties
        yield "pins", self.pins

    @classmethod
    def place(
        cls,
        unit: UnitData,
        at: At,
        *,
        in_bom: bool | None = None,
        on_board: bool | None = None,
        mirror: Mirror | None = None,
        uuid: UUID | None = None,
    ):
        rv = cls(
            lib_id=unit.lib_id,
            at=at,
            mirror=mirror,
            unit=unit.unit,
            convert=unit.convert if unit.convert != 1 else None,
            in_bom=in_bom if in_bom is not None else unit.in_bom,
            on_board=on_board if on_board is not None else unit.on_board,
            fields_autoplaced=True,
            uuid=uuid,
            properties=(
                prop.copy(at=(at + prop.at).round())
                for prop in unit.properties.iter_values()
                if not prop.name.startswith("ki_")
            ),
            pins=((num, UUID()) for num in unit.pins),
            unit_data=unit,
        )
        rv.properties["Reference"] += "?"
        return rv

    @property
    def _unit_data(self) -> UnitData:
        if self.__unit_data is None:
            self.__unit_data = t.cast(
                UnitData,
                self.parent.lib_symbols[self.lib_id].unit(self.unit),  # type: ignore
            )
        return self.__unit_data

    def pin_at(self, pin: str) -> At:
        pin_at = self._unit_data.pin_at(pin)
        match self.mirror:
            case Symbol.Mirror.X:
                pin_at = pin_at.mirror_x
            case Symbol.Mirror.Y:
                pin_at = pin_at.mirror_y
            case _:
                pass
        return (self.at + pin_at).round()

    def set_reference(
        self,
        ref_id: t.Any,
        *,
        at: At | None = None,
        effects: Effects | None | t.Literal[DEFAULT] = DEFAULT,
    ) -> str:
        unit_ref = self._unit_data.properties["Reference"]
        newref = f"{unit_ref}{ref_id}"
        self.properties.set_property("Reference", newref, at=at, effects=effects)
        return newref
