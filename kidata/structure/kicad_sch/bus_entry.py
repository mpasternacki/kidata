from __future__ import annotations

import typing as t

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ...api import DEFAULT
from ...typing import KiValue
from .._helpers import extract_coords
from ..at import At
from ..graphical import Stroke
from ..structure import Structure
from ..uuid import UUID


@t.final
class BusEntry(Structure, car="bus_entry"):

    __slots__ = ("at", "size", "stroke", "uuid")
    at: t.Final[At]
    size: t.Final[tuple[float, float]]
    stroke: t.Final[Stroke]
    uuid: t.Final[UUID]

    def __init__(
        self,
        at: At,
        size: tuple[float, float],
        *,
        stroke: Stroke | t.Literal[DEFAULT] = DEFAULT,
        uuid: UUID | t.Literal[DEFAULT] = DEFAULT,
    ):
        self.at = at
        self.size = size
        self.stroke = stroke if stroke is not DEFAULT else Stroke()
        self.uuid = uuid if uuid is not DEFAULT else UUID()
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.at.copy(), self.size, stroke=self.stroke.copy(), uuid=self.uuid.copy()
        )

    __copy__ = copy

    def iter_children(self):
        yield self.at
        yield self.stroke
        yield self.uuid

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (at,) = At.extract_from(data)
        size = extract_coords(data, "size")
        (stroke,) = Stroke.extract_from(data)
        (uuid,) = UUID.extract_from(data)
        return cls(at, size=size, stroke=stroke, uuid=uuid)

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...]]:
        yield self.at.unangled
        yield "size", *self.size
        yield self.stroke
        yield self.uuid

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.at
        yield "size", self.size
        yield "stroke", self.stroke
        yield "uuid", self.uuid
