from __future__ import annotations

import typing as t

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ...format import kiformat_list
from ...list import List
from ...typing import KiValue


class Nets(dict[int, str]):
    __slots__ = ()

    @classmethod
    def extract_from(cls, data: list[KiValue]) -> Self:
        def _kv_gen() -> t.Iterator[tuple[int, str]]:
            for property in List.extract_from(data, "net"):
                (key, val) = property.cdr
                assert isinstance(key, int)
                assert isinstance(val, str)
                yield (key, val)

        return cls(_kv_gen())

    def _kiformat_(self, f: t.TextIO) -> None:
        for kv in self.items():
            kiformat_list("net", kv, f)

    def copy(self) -> Self:
        return self.__class__(self.items())

    __copy__ = copy

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({super().__repr__()})"

    __str__ = __repr__

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield None, list(self.items())
