from __future__ import annotations

import time
import typing as t
from copy import deepcopy

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ..._util import extract
from ...bareword import Bareword
from ...list import List
from ...typing import KiValue
from .._helpers import extract_bareword_flag, extract_value, must, pop_value
from ..at import At
from ..properties import Properties
from ..structure import Structure
from .pad import Pad
from .tstamp import TStamp

_BW_locked = Bareword("locked")
_BW_placed = Bareword("placed")


def _is_graphical(elt: KiValue) -> t.TypeGuard[List]:
    return isinstance(elt, List) and elt.car.startswith("fp_")


@t.final
class Footprint(Structure, car="footprint", allow_extra_list_data=True):
    __slots__ = (
        "library_link",
        "locked",
        "placed",
        "layer",
        "tedit",
        "tstamp",
        "at",
        "descr",
        "tags",
        "properties",
        "path",
        "rest",
        "attr",
        "graphical",
        "pads",
        "zones",
        "groups",
        "model",
    )
    library_link: str
    locked: bool
    placed: bool
    layer: str
    tedit: Bareword  # TODO: structure
    tstamp: TStamp
    at: At
    descr: str | None
    tags: str | None
    properties: Properties
    path: str | None
    rest: list[KiValue]
    attr: list[List]  # TODO: structure
    graphical: list[List]
    pads: list[Pad]
    zones: list[List]
    groups: list[List]
    model: list[List]

    def __init__(
        self,
        library_link: str,
        *,
        locked: bool = False,
        placed: bool = False,
        layer: str,
        tedit: Bareword | str | None = None,
        tstamp: TStamp.From = None,
        at: At,
        descr: str | None = None,
        tags: str | None = None,
        properties: Properties.From = None,
        path: str | None = None,
        rest: list[KiValue] | None = None,
        attr: t.Iterable[List] = (),
        graphical: t.Iterable[List] = (),
        pads: t.Iterable[Pad] = (),
        zones: t.Iterable[List] = (),
        groups: t.Iterable[List] = (),
        model: t.Iterable[List] = (),
    ):
        if tedit is None:
            tedit = Bareword(f"{int(time.time()):X}")
        elif not isinstance(tedit, Bareword):
            tedit = Bareword(tedit)

        self.library_link = library_link
        self.locked = locked
        self.placed = placed
        self.layer = layer
        self.tedit = tedit
        self.tstamp = TStamp.from_(tstamp)
        self.at = at
        self.descr = descr
        self.tags = tags
        self.properties = Properties.from_(properties)
        self.path = path
        self.rest = rest or []
        self.attr = list(attr)
        self.graphical = list(graphical)
        self.pads = list(pads)
        self.zones = list(zones)
        self.groups = list(groups)
        self.model = list(model)
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.library_link,
            locked=self.locked,
            placed=self.placed,
            layer=self.layer,
            tedit=self.tedit,
            tstamp=self.tstamp.copy(),
            at=self.at.copy(),
            descr=self.descr,
            tags=self.tags,
            properties=self.properties.copy(),
            path=self.path,
            rest=[deepcopy(elt) for elt in self.rest],
            attr=[deepcopy(elt) for elt in self.attr],
            graphical=[deepcopy(elt) for elt in self.graphical],
            pads=(pad.copy() for pad in self.pads),
            zones=[deepcopy(elt) for elt in self.zones],
            groups=[deepcopy(elt) for elt in self.groups],
            model=(deepcopy(elt) for elt in self.model),
        )

    def iter_children(self):
        yield self.tstamp
        yield self.at
        yield from self.pads

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        library_link = pop_value(data, str)
        locked = extract_bareword_flag(data, _BW_locked)
        placed = extract_bareword_flag(data, _BW_placed)
        layer = must(extract_value(data, "layer", str))
        tedit = must(extract_value(data, "tedit", Bareword))
        (tstamp,) = TStamp.extract_from(data)
        (at,) = At.extract_from(data)
        descr = extract_value(data, "descr", str)
        tags = extract_value(data, "tags", str)
        properties = Properties.extract_from(data)
        path = extract_value(data, "path", str)
        attr = List.extract_from(data, "attr")
        graphical = extract(
            data,
            lambda elt: elt if _is_graphical(elt) else None,
        )
        pads = Pad.extract_from(data)
        zones = List.extract_from(data, "zone")
        groups = List.extract_from(data, "group")
        model = List.extract_from(data, "model")

        return cls(
            library_link,
            locked=locked,
            placed=placed,
            layer=layer,
            tedit=tedit,
            tstamp=tstamp,
            at=at,
            descr=descr,
            tags=tags,
            properties=properties,
            path=path,
            rest=data,
            attr=attr,
            graphical=graphical,
            pads=pads,
            zones=zones,
            groups=groups,
            model=model,
        )

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        yield self.library_link
        if self.locked:
            yield _BW_locked
        if self.placed:
            yield _BW_placed
        yield "layer", self.layer
        yield "tedit", self.tedit
        yield self.tstamp
        yield self.at
        if self.descr is not None:
            yield "descr", self.descr
        if self.tags is not None:
            yield "tags", self.tags
        yield self.properties
        if self.path is not None:
            yield "path", self.path
        yield from self.rest
        yield from self.attr
        yield from self.graphical
        yield from self.pads
        yield from self.zones
        yield from self.groups
        yield from self.model

    def pad(self, number: str) -> Pad | None:
        for pad in self.pads:
            if pad.number == number:
                return pad
        return None

    def pad_at(self, number: str) -> At | None:
        if (pad := self.pad(number)) is None:
            return None
        return self.at.copy(angle=-self.at.angle if self.at.angle is not None else None) + pad.at
