from __future__ import annotations

from ..uuid import UUID


class TStamp(UUID, car="tstamp"):
    __slots__ = ()
