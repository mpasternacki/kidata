from __future__ import annotations

import typing as t
from copy import deepcopy

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ...bareword import Bareword
from ...list import List
from ...typing import KiValue
from .._helpers import (
    extract_bareword_flag,
    extract_cdr,
    extract_coords,
    extract_empty_list_flag,
    extract_value,
    must,
    only,
    pop_value,
)
from ..at import At
from ..properties import Properties
from ..structure import Structure
from .tstamp import TStamp

_BW_locked = Bareword("locked")


@t.final
class Pad(Structure, car="pad", allow_extra_list_data=True):
    __slots__ = (
        "number",
        "kind",
        "shape",
        "at",
        "locked",
        "size",
        "drill",
        "layers",
        "properties",
        "remove_unused_layers",
        "keep_end_layers",
        "roundrect_rratio",
        "chamfer_ratio",
        "chamfer",
        "net",
        "tstamp",
        "rest",
    )
    number: str
    kind: Bareword  # TODO: enum
    shape: Bareword  # TODO: enum
    at: At
    locked: bool
    size: tuple[float, float]
    drill: List | None  # TODO: structure
    layers: tuple[str, ...]
    properties: Properties
    remove_unused_layers: t.Final[bool]
    keep_end_layers: t.Final[bool]
    roundrect_rratio: t.Final[float | None]
    chamfer_ratio: t.Final[float | None]
    chamfer: tuple[Bareword, ...]  # TODO: enum flag
    net: tuple[int, str] | None
    tstamp: TStamp
    rest: list[KiValue]

    def __init__(
        self,
        number: str,
        *,
        kind: Bareword.From,
        shape: Bareword.From,
        at: At,
        locked: bool = False,
        size: tuple[float, float],
        drill: List | None = None,
        layers: t.Iterable[str],
        properties: Properties.From = None,
        remove_unused_layers: bool = False,
        keep_end_layers: bool = False,
        roundrect_rratio: float | None = None,
        chamfer_ratio: float | None = None,
        chamfer: t.Iterable[Bareword.From] = (),
        net: tuple[int, str] | None,
        tstamp: TStamp.From,
        rest: t.Iterable[KiValue] = (),
    ):
        self.number = number
        self.kind = Bareword.from_(kind)
        self.shape = Bareword.from_(shape)
        self.at = at
        self.locked = locked
        self.size = size
        self.drill = drill
        self.layers = tuple(layers)
        self.properties = Properties.from_(properties)
        self.remove_unused_layers = remove_unused_layers
        self.keep_end_layers = keep_end_layers
        self.roundrect_rratio = roundrect_rratio
        self.chamfer_ratio = chamfer_ratio
        self.chamfer = tuple(Bareword.from_(elt) for elt in chamfer)
        self.net = net
        self.tstamp = TStamp.from_(tstamp)
        self.rest = list(rest)

        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.number,
            kind=self.kind,
            shape=self.shape,
            at=self.at.copy(),
            locked=self.locked,
            size=self.size,
            drill=deepcopy(self.drill),
            layers=self.layers,
            properties=self.properties.copy(),
            remove_unused_layers=self.remove_unused_layers,
            keep_end_layers=self.keep_end_layers,
            roundrect_rratio=self.roundrect_rratio,
            chamfer_ratio=self.chamfer_ratio,
            chamfer=self.chamfer,
            net=self.net,
            tstamp=self.tstamp.copy(),
            rest=(deepcopy(elt) for elt in self.rest),
        )

    def iter_children(self):
        yield self.tstamp
        yield self.at

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        def _gen_layers(layers: t.Iterable[KiValue]) -> t.Iterator[str]:
            for layer in layers:
                if isinstance(layer, Bareword):
                    yield layer.value
                elif isinstance(layer, str):
                    yield layer
                else:
                    raise ValueError(
                        f"Layer must be bareword or str, got {layer.__class__} ({layer!r})"
                    )

        def _gen_chamfer(data2: t.Iterable[KiValue] | None) -> t.Iterator[Bareword]:
            if data2 is None:
                return
            for elt in data2:
                assert isinstance(elt, Bareword)
                yield elt

        number = pop_value(data, str)
        kind = pop_value(data, Bareword)
        shape = pop_value(data, Bareword)
        (at,) = At.extract_from(data)
        locked = extract_bareword_flag(data, "locked")
        size = extract_coords(data, "size")
        drill = only(List.extract_from(data, "drill"))
        layers = _gen_layers(must(extract_cdr(data, "layers")))
        properties = Properties.extract_from(data)
        remove_unused_layers = extract_empty_list_flag(data, "remove_unused_layers")
        keep_end_layers = extract_empty_list_flag(data, "keep_end_layers")
        roundrect_rratio = extract_value(data, "roundrect_rratio", float)
        chamfer_ratio = extract_value(data, "chamfer_ratio", float)
        chamfer = _gen_chamfer(extract_cdr(data, "chamfer"))
        net: tuple[int, str] | None = None
        if (net_cdr := extract_cdr(data, "net")) is not None:
            (net_number, net_name) = net_cdr
            assert isinstance(net_number, int)
            assert isinstance(net_name, str)
            net = (net_number, net_name)
        (tstamp,) = TStamp.extract_from(data)
        return cls(
            number,
            kind=kind,
            shape=shape,
            at=at,
            locked=locked,
            size=size,
            drill=drill,
            layers=layers,
            properties=properties,
            remove_unused_layers=remove_unused_layers,
            keep_end_layers=keep_end_layers,
            roundrect_rratio=roundrect_rratio,
            chamfer_ratio=chamfer_ratio,
            chamfer=chamfer,
            net=net,
            tstamp=tstamp,
            rest=data,
        )

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        yield self.number
        yield self.kind
        yield self.shape
        yield self.at
        if self.locked:
            yield _BW_locked
        yield "size", *self.size
        yield self.drill
        yield "layers", *self.layers
        yield self.properties
        if self.remove_unused_layers:
            yield "remove_unused_layers",
        if self.keep_end_layers:
            yield "self.keep_end_layers",
        if self.roundrect_rratio is not None:
            yield "roundrect_rratio", self.roundrect_rratio
        if self.chamfer_ratio is not None:
            yield "chamfer_ratio", self.chamfer_ratio
        if self.chamfer:
            yield "chamfer", *self.chamfer
        if self.net:
            yield "net", *self.net
        yield self.tstamp
        yield from self.rest
