from __future__ import annotations

import typing as t
import uuid

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ...bareword import Bareword
from ...typing import KiValue
from .._helpers import (
    extract_bareword_flag,
    extract_cdr,
    extract_coords,
    extract_empty_list_flag,
    extract_value,
    must,
    pop_value,
)
from ..at import At
from ..structure import Structure
from .tstamp import TStamp

_BW_locked = Bareword("locked")


class Track(Structure):
    __slots__ = ("locked", "net", "tstamp")
    locked: bool
    net: int
    tstamp: TStamp

    def __init__(
        self, *, net: int, locked: bool = False, tstamp: TStamp | uuid.UUID | str | None = None
    ):
        self.net = net
        self.locked = locked
        self.tstamp = tstamp if isinstance(tstamp, TStamp) else TStamp(tstamp)
        super().__init__()

    def iter_children(self) -> t.Iterator[Structure]:
        yield self.tstamp


class Segment(Track, car="segment"):
    __slots__ = ("start", "end", "width", "layer")
    start: t.Final[tuple[float, float]]
    end: t.Final[tuple[float, float]]
    width: t.Final[float]
    layer: t.Final[str]

    def __init__(
        self,
        start: tuple[float, float],
        end: tuple[float, float],
        *,
        width: float = 0.25,
        layer: str = "B.Cu",
        net: int,
        locked: bool = False,
        tstamp: TStamp | uuid.UUID | str | None = None,
    ):
        self.start = start
        self.end = end
        self.width = width
        self.layer = layer
        super().__init__(net=net, locked=locked, tstamp=tstamp)

    def copy(self) -> Self:
        return self.__class__(
            self.start,
            self.end,
            width=self.width,
            layer=self.layer,
            net=self.net,
            locked=self.locked,
            tstamp=self.tstamp.copy(),
        )

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        start = extract_coords(data, "start")
        end = extract_coords(data, "end")
        width = must(extract_value(data, "width", float))
        layer = must(extract_value(data, "layer", str))
        net = must(extract_value(data, "net", int))
        locked = extract_bareword_flag(data, _BW_locked)
        (tstamp,) = TStamp.extract_from(data)
        return cls(start, end, width=width, layer=layer, net=net, locked=locked, tstamp=tstamp)

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        if self.locked:
            yield _BW_locked
        yield "start", *self.start
        yield "end", *self.end
        yield "width", self.width
        yield "layer", self.layer
        yield "net", self.net
        yield self.tstamp


@t.final
class Via(Track, car="via"):
    __slots__ = (
        "kind",
        "at",
        "size",
        "drill",
        "layers",
        "remove_unused_layers",
        "keep_end_layers",
        "free",
    )
    kind: t.Final[Bareword | None]
    at: t.Final[At]
    size: t.Final[float]
    drill: t.Final[float]
    layers: t.Final[tuple[str, ...]]
    remove_unused_layers: t.Final[bool]
    keep_end_layers: t.Final[bool]
    free: t.Final[bool]

    def __init__(
        self,
        at: At,
        *,
        size: float,
        drill: float,
        layers: t.Iterable[str],
        remove_unused_layers: bool = False,
        keep_end_layers: bool = False,
        free: bool = False,
        net: int,
        kind: Bareword | str | None = None,
        locked: bool = False,
        tstamp: TStamp | uuid.UUID | str | None = None,
    ):
        if kind is not None and not isinstance(kind, Bareword):
            kind = Bareword(kind)

        self.at = at
        self.size = size
        self.drill = drill
        self.layers = tuple(layers)
        self.remove_unused_layers = remove_unused_layers
        self.keep_end_layers = keep_end_layers
        self.free = free
        self.kind = kind
        super().__init__(net=net, locked=locked, tstamp=tstamp)

    def copy(self) -> Self:
        return self.__class__(
            self.at.copy(),
            size=self.size,
            drill=self.drill,
            layers=self.layers,
            remove_unused_layers=self.remove_unused_layers,
            keep_end_layers=self.keep_end_layers,
            free=self.free,
            kind=self.kind,
            net=self.net,
            locked=self.locked,
            tstamp=self.tstamp.copy(),
        )

    def iter_children(self):
        yield self.at
        yield from super().iter_children()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (at,) = At.extract_from(data)
        size = must(extract_value(data, "size", float))
        drill = must(extract_value(data, "drill", float))
        layers_cdr = must(extract_cdr(data, "layers"))
        remove_unused_layers = extract_bareword_flag(data, "remove_unused_layers")
        keep_end_layers = extract_bareword_flag(data, "keep_end_layers")
        free = extract_empty_list_flag(data, "free")
        net = must(extract_value(data, "net", int))
        locked = extract_bareword_flag(data, _BW_locked)
        (tstamp,) = TStamp.extract_from(data)
        kind = pop_value(data, Bareword) if len(data) else None

        def _layers_gen() -> t.Iterator[str]:
            for layer in layers_cdr:
                assert isinstance(layer, str)
                yield layer

        return cls(
            at,
            size=size,
            drill=drill,
            layers=_layers_gen(),
            remove_unused_layers=remove_unused_layers,
            keep_end_layers=keep_end_layers,
            free=free,
            kind=kind,
            net=net,
            locked=locked,
            tstamp=tstamp,
        )

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        yield self.kind
        yield self.at
        if self.locked:
            yield _BW_locked
        yield "size", self.size
        yield "drill", self.drill
        yield "layers", *self.layers
        if self.remove_unused_layers:
            yield "remove_unused_layers",
        if self.keep_end_layers:
            yield "keep_end_layers",
        if self.free:
            yield "free",
        yield "net", self.net
        yield self.tstamp


class Arc(Segment, car="arc"):
    __slots__ = ("mid",)
    mid: t.Final[tuple[float, float]]

    def __init__(
        self,
        start: tuple[float, float],
        mid: tuple[float, float],
        end: tuple[float, float],
        *,
        width: float = 0.25,
        layer: str = "B.Cu",
        net: int,
        locked: bool = False,
        tstamp: TStamp | uuid.UUID | str | None = None,
    ):
        self.mid = mid
        super().__init__(
            start, end, width=width, layer=layer, net=net, locked=locked, tstamp=tstamp
        )

    def copy(self) -> Self:
        return self.__class__(
            self.start,
            self.mid,
            self.end,
            width=self.width,
            layer=self.layer,
            net=self.net,
            locked=self.locked,
            tstamp=self.tstamp.copy(),
        )

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        start = extract_coords(data, "start")
        mid = extract_coords(data, "mid")
        end = extract_coords(data, "end")
        width = must(extract_value(data, "width", float))
        layer = must(extract_value(data, "layer", str))
        net = must(extract_value(data, "net", int))
        locked = extract_bareword_flag(data, _BW_locked)
        (tstamp,) = TStamp.extract_from(data)
        return cls(start, mid, end, width=width, layer=layer, net=net, locked=locked, tstamp=tstamp)

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        super_cdr_iter = iter(super().cdr)
        yield next(super_cdr_iter)  # start
        yield "mid", *self.mid
        yield from super_cdr_iter
