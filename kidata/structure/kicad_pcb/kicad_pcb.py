from __future__ import annotations

import typing as t
from copy import deepcopy

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ..._util import extract
from ...api import DEFAULT
from ...list import List
from ...typing import KiValue
from .._helpers import only
from ..generator import Generator
from ..paper import Paper
from ..parseable_structure import ParseableStructure
from ..properties import Properties
from ..structure import Structure
from ..title_block import TitleBlock
from ..version import Version
from .footprint import Footprint
from .nets import Nets
from .track import Track


def _is_graphical(elt: KiValue) -> t.TypeGuard[List]:
    return isinstance(elt, List) and (elt.car.startswith("gr_") or elt.car == "dimension")


@t.final
class KicadPcb(
    ParseableStructure, car="kicad_pcb", suffix=".kicad_pcb", allow_extra_list_data=True
):
    __slots__ = (
        "version",
        "generator",
        "general",
        "paper",
        "title_block",
        "layers",
        "setup",
        "nets",
        "properties",
        "footprints",
        "graphical",
        "targets",
        "tracks",
        "zones",
        "groups",
        "rest",
    )
    version: Version
    generator: Generator
    general: List
    paper: Paper
    title_block: TitleBlock | None
    layers: List  # TODO: structure
    setup: List  # TODO: structure (or alist)
    nets: Nets
    properties: Properties
    footprints: list[Footprint]
    graphical: list[List]  # TODO: structure
    targets: list[List]  # TODO: structure !!
    tracks: list[Track]
    zones: list[List]  # TODO: structure
    groups: list[List]  # TODO: structure
    rest: list[KiValue]

    def __init__(
        self,
        *,
        version: Version | t.Literal[DEFAULT] = DEFAULT,
        generator: Generator | str | t.Literal[DEFAULT] = DEFAULT,
        general: List | None = None,
        paper: Paper | str = Paper.DEFAULT_SIZE,
        title_block: TitleBlock | None = None,
        layers: List | None = None,
        setup: List | None = None,
        nets: Nets | t.Mapping[int, str] | t.Iterable[tuple[int, str]] | None = None,
        properties: Properties | t.Mapping[str, str] | t.Iterable[tuple[str, str]] | None = None,
        footprints: t.Iterable[Footprint] = (),
        graphical: list[List] | None = None,
        targets: list[List] | None = None,
        tracks: t.Iterable[Track] = (),
        zones: list[List] | None = None,
        groups: list[List] | None = None,
        rest: list[KiValue] | None = None,
    ):
        if not generator:
            generator = Generator()
        elif isinstance(generator, str):
            generator = Generator(generator)

        if isinstance(paper, str):
            paper = Paper(paper)

        if nets is None:
            nets = Nets()
        elif not isinstance(nets, Nets):
            nets = Nets(nets)

        if properties is None:
            properties = Properties()
        elif not isinstance(properties, Properties):
            properties = Properties(properties)

        self.version = version if version is not DEFAULT else Version()
        self.generator = generator if generator is not DEFAULT else Generator()
        self.paper = paper
        self.title_block = title_block
        self.general = general or List("general")
        self.layers = layers or List("layers")
        self.setup = setup or List("setup")
        self.nets = nets
        self.properties = properties
        self.footprints = list(footprints)
        self.graphical = graphical or []
        self.targets = targets or []
        self.tracks = list(tracks)
        self.zones = zones or []
        self.groups = groups or []
        self.rest = rest or []
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            version=self.version.copy(),
            generator=self.generator.copy(),
            general=deepcopy(self.general),
            paper=self.paper.copy(),
            title_block=self.title_block.copy() if self.title_block else None,
            layers=deepcopy(self.layers),
            setup=deepcopy(self.setup),
            nets=self.nets.copy(),
            properties=self.properties.copy(),
            footprints=(elt.copy() for elt in self.footprints),
            graphical=[deepcopy(elt) for elt in self.graphical],
            targets=[deepcopy(elt) for elt in self.targets],
            tracks=(track.copy() for track in self.tracks),
            zones=[deepcopy(elt) for elt in self.zones],
            groups=[deepcopy(elt) for elt in self.groups],
            rest=[elt.copy() if isinstance(elt, Structure) else elt for elt in self.rest],
        )

    __copy__ = copy

    def iter_children(self):
        yield self.version
        yield self.generator
        yield self.paper
        if self.title_block:
            yield self.title_block
        for elt in self.rest:
            if isinstance(elt, Structure):
                yield elt
        yield from self.tracks

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (version,) = Version.extract_from(data)
        (generator,) = Generator.extract_from(data)
        (general,) = List.extract_from(data, "general")
        (paper,) = Paper.extract_from(data)
        title_block = only(TitleBlock.extract_from(data))
        (layers,) = List.extract_from(data, "layers")
        (setup,) = List.extract_from(data, "setup")
        nets = Nets.extract_from(data)
        properties = Properties.extract_from(data)
        footprints = Footprint.extract_from(data)
        graphical = list(
            extract(
                data,
                lambda elt: elt if _is_graphical(elt) else None,
            )
        )
        targets = list(List.extract_from(data, "target"))
        tracks = Track.extract_from(data)
        zones = list(List.extract_from(data, "zone"))
        groups = list(List.extract_from(data, "group"))

        return cls(
            version=version,
            generator=generator,
            general=general,
            paper=paper,
            title_block=title_block,
            layers=layers,
            setup=setup,
            nets=nets,
            properties=properties,
            footprints=footprints,
            graphical=graphical,
            targets=targets,
            tracks=tracks,
            zones=zones,
            groups=groups,
            rest=data,
        )

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        yield self.version
        yield self.generator
        yield self.general
        yield self.paper
        yield self.title_block
        yield self.layers
        yield self.setup
        yield self.nets
        yield self.properties
        yield from self.footprints
        yield from self.graphical
        yield from self.targets
        yield from self.tracks
        yield from self.zones
        yield from self.groups
        yield from self.rest
