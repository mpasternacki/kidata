from __future__ import annotations

import typing as t
from collections.abc import MutableMapping

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ..api import DEFAULT
from .at import At
from .graphical import Effects
from .property import Property
from .structure import Structure


@t.final
class SymbolProperties(MutableMapping[str, str]):
    __slots__ = (
        "_ids",
        "_properties",
        "_parent",
    )
    _ids: set[int]
    _properties: dict[str, Property]
    _parent: Structure | None

    def __init__(self, properties: t.Iterable[Property] = (), *, parent: Structure | None = None):
        self._ids = set()
        self._properties = {}
        self._parent = parent

        for property in properties:
            assert property.name not in self._properties
            self.add(property)

    def copy_values(self) -> t.Iterator[Property]:
        for prop in self._properties.values():
            yield prop.copy()

    def copy(self) -> Self:
        return self.__class__(self.copy_values())

    __copy__ = copy

    def iter_values(self) -> t.Iterator[Property]:
        yield from self._properties.values()

    def _next_id(self) -> int:
        return max(max(self._ids) + 1, Property.MIN_USER_ID)

    def add(self, prop: Property) -> Property:
        if prop.id in self._ids:
            if (old := self._properties.get(prop.name)) is None or old.id != prop.id:
                next_id = self._next_id()
                prop = prop.copy(id=next_id)
        prop.parent = self._parent
        self._properties[prop.name] = prop
        self._ids.add(prop.id)
        return prop

    def __getitem__(self, k: str) -> str:
        return self._properties[k].value

    def __iter__(self) -> t.Iterator[str]:
        return iter(self._properties)

    def __len__(self) -> int:
        return len(self._properties)

    def __setitem__(self, k: str, v: str) -> None:
        self.set_property(k, v)

    def __delitem__(self, k: str):
        prop = self._properties[k]
        self._ids.remove(prop.id)
        del self._properties[k]

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield from self.iter_values()

    def get_property(self, k: str) -> Property | None:
        return self._properties.get(k)

    def hide(self, k: str) -> Property:
        return self.add(self._properties[k].hidden())

    def move(self, k: str, at: At) -> Property:
        return self.add(self._properties[k].copy(at=at))

    def set_property(
        self,
        k: str,
        v: str | None = None,
        *,
        at: At | None = None,
        effects: Effects | None | t.Literal[DEFAULT] = DEFAULT,
    ):
        if (prop := self._properties.get(k)) is not None:
            self.add(prop.copy(value=v, at=at, effects=effects))
        else:
            new_id = Property.FIXED_IDS[k] if k in Property.FIXED_IDS else self._next_id()
            self.add(Property(k, v or "", id=new_id, at=at, effects=effects))
