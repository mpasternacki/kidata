from __future__ import annotations

import typing as t

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ...typing import KiValue
from .._helpers import extract_bareword_flag, extract_value
from .pin_numbers import PinNumbers


class PinNames(PinNumbers, car="pin_names"):
    __slots__ = ("offset",)
    offset: t.Final[float | None]

    def __init__(self, hide: bool, offset: float | None = None):
        self.offset = offset
        super().__init__(hide=hide)

    def copy(self) -> Self:
        return self.__class__(hide=self.hide, offset=self.offset)

    __copy__ = copy

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        hide = extract_bareword_flag(data, "hide")
        offset = extract_value(data, "offset", float)
        return cls(hide=hide, offset=offset)

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...]]:
        if self.offset is not None:
            yield "offset", self.offset
        yield from super().cdr

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield from super().__rich_repr__()
        yield "offset", self.offset, None
