from __future__ import annotations

import typing as t
from inspect import signature

from ..._util import Self
from ...typing import KiValue
from .._container import ContainedStructure
from .._helpers import extract_value
from ..property import Property
from .flat_symbol import FlatSymbol
from .pin_names import PinNames
from .pin_numbers import PinNumbers
from .symbol_base import SymbolBase
from .symbol_unit import SymbolUnit


@t.final
class Symbol(SymbolBase, car="symbol"):
    """A (symbol) in (kicad_symbol_lib) of .kicad_sym file"""

    Container = ContainedStructure.Container[str, Self]

    __slots__ = ("_extends",)
    _extends: str | None

    def __init__(
        self,
        name: str,
        *,
        extends: str | None = None,
        power: bool = False,
        pin_numbers: PinNumbers | None = None,
        pin_names: PinNames | None = None,
        in_bom: bool | None = True,
        on_board: bool | None = True,
        properties: t.Iterable[Property] = (),
        units: t.Iterable[SymbolUnit] = (),
    ):
        self._extends = extends
        super().__init__(
            name,
            power=power,
            pin_numbers=pin_numbers,
            pin_names=pin_names,
            in_bom=in_bom,
            on_board=on_board,
            properties=properties,
            units=units,
        )

    def copy(self) -> Self:
        rv = super().copy()
        rv._extends = self._extends
        return rv

    __copy__ = copy

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        rv = super().from_list_data(data)
        rv._extends = extract_value(data, "extends", str)  # hack hack
        return rv

    @property
    def extends(self) -> Self | None:
        if self._extends:
            return self.container[self._extends]
        return None

    @property
    def cdr(self):
        super_cdr = iter(super().cdr)
        yield next(super_cdr)  # name
        if self._extends:
            yield "extends", self._extends
        yield from super_cdr

    def __rich_repr__(self):
        super_repr = iter(super().__rich_repr__())
        yield next(super_repr)  # name
        yield "extends", self._extends, None
        yield from super_repr

    def flatten(self, library_nickname: str | None = None) -> FlatSymbol:
        self.unlazificate()
        parent = self.extends
        if parent:
            parent.unlazificate()
            if parent._extends:
                raise NotImplementedError(
                    "Nested (extends) not supported in"
                    f" {self.name} -> {parent.name} -> {parent._extends}"
                )

        kwargs: dict[str, t.Any] = {}
        kwargs["power"] = self.power or (parent and parent.power)

        if self.pin_numbers:
            kwargs["pin_numbers"] = self.pin_numbers.copy()
        elif parent and parent.pin_numbers:
            kwargs["pin_numbers"] = parent.pin_numbers.copy()
        else:
            kwargs["pin_numbers"] = None

        if self.pin_names:
            kwargs["pin_names"] = self.pin_names.copy()
        elif parent and parent.pin_names:
            kwargs["pin_names"] = parent.pin_names.copy()
        else:
            kwargs["pin_names"] = None

        kwargs["in_bom"] = self.in_bom if self.in_bom is not None else (parent and parent.in_bom)
        kwargs["on_board"] = (
            self.on_board if self.on_board is not None else (parent and parent.on_board)
        )

        # TODO: merge in Properties somehow? Avoid discarding this Properties instance?
        props = self.properties.copy()
        if parent:
            for prop in parent.properties.iter_values():
                if prop.name not in props:
                    props.add(prop)
        kwargs["properties"] = props.iter_values()

        units = {k: unit.copy() for k, unit in self.units.items()}
        if parent:
            for k, unit in parent.units.items():
                if k not in units:
                    units[k] = unit.copy(self.name)
        kwargs["units"] = units.values()

        bound = signature(FlatSymbol).bind(
            f"{library_nickname}:{self.name}" if library_nickname is not None else self.name,
            **kwargs,
        )
        return FlatSymbol(*bound.args, **bound.kwargs)
