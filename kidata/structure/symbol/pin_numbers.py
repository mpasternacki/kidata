from __future__ import annotations

import typing as t

if t.TYPE_CHECKING:
    from typing_extensions import Self

from ...bareword import Bareword
from ...typing import KiValue
from .._helpers import extract_bareword_flag
from ..structure import Structure


class PinNumbers(Structure, car="pin_numbers"):
    __HIDE_BW: t.Final[Bareword] = Bareword("hide")

    __slots__ = ("hide",)
    hide: t.Final[bool]

    def __init__(self, hide: bool):
        self.hide = hide
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(hide=self.hide)

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        return cls(hide=extract_bareword_flag(data, "hide"))

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...]]:
        if self.hide:
            yield self.__HIDE_BW

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield "hide", self.hide

    @classmethod
    @property
    def HIDE(cls) -> Self:
        return cls(hide=True)
