from __future__ import annotations

import typing as t
from warnings import warn

from ..._util import Self
from ...typing import KiValue
from .._helpers import pop_value
from ..graphical import Graphical
from ..structure import Structure
from .pin import Pin


class SymbolUnit(Structure, car="symbol"):
    __slots__ = ("lib_id", "unit", "style", "graphical", "pins")
    lib_id: str
    unit: int
    style: int
    graphical: list[Graphical]
    pins: dict[str, Pin]

    @staticmethod
    def split_name(name: str) -> tuple[str, int, int]:
        lib_id, unit, style = name.rsplit("_", 2)
        return (lib_id, int(unit), int(style))

    def __init__(
        self,
        lib_id: str,
        unit: int = 0,
        style: int = 0,
        *,
        graphical: t.Iterable[Graphical] = (),
        pins: t.Iterable[Pin] = (),
    ):
        pins_dict: dict[str, Pin] = {}
        for pin in pins:
            if pin.number in pins_dict:
                warn(
                    f"Duplicate pin number {pin.number!r} in {lib_id}_{unit}_{style}."
                    f" Keeping first {pins_dict[pin.number].name!r}, discarding new {pin.name!r}"
                )
                continue
            pins_dict[pin.number] = pin

        self.graphical = list(graphical)
        self.pins = pins_dict
        self.lib_id = lib_id
        self.unit = unit
        self.style = style
        super().__init__()

    def copy(self, lib_id: str | None = None) -> Self:
        return self.__class__(
            lib_id if lib_id is not None else self.lib_id,
            unit=self.unit,
            style=self.style,
            graphical=(gr.copy() for gr in self.graphical),
            pins=(pin.copy() for pin in self.pins.values()),
        )

    __copy__ = copy

    @property
    def name(self) -> str:
        return f"{self.lib_id}_{self.unit}_{self.style}"

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        name = pop_value(data, str)
        lib_id, unit, style = cls.split_name(name)
        graphical = Graphical.extract_from(data)
        pins = Pin.extract_from(data)

        return cls(
            lib_id,
            unit,
            style,
            graphical=graphical,
            pins=pins,
        )

    @property
    def cdr(self) -> t.Iterable[KiValue | None]:
        yield self.name
        yield from self.graphical
        yield from self.pins.values()

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.name
        yield "graphical", self.graphical, []
        yield "pins", self.pins, []

    def iter_children(self):
        yield from self.graphical
        yield from self.pins.values()
