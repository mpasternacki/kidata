from __future__ import annotations

import typing as t

from ..._util import Self
from ...bareword import Bareword
from ...list import List
from ...typing import KiValue
from .._helpers import BarewordEnum, extract_bareword_flag, extract_value, must, only, pop_value
from ..at import At
from ..graphical import Effects
from ..structure import Structure


class PinElectricalType(BarewordEnum):
    INPUT = "input"
    OUTPUT = "output"
    BIDIRECTIONAL = "bidirectional"
    TRI_STATE = "tri_state"
    PASSIVE = "passive"
    FREE = "free"
    UNSPECIFIED = "unspecified"
    POWER_IN = "power_in"
    POWER_OUT = "power_out"
    OPEN_COLLECTOR = "open_collector"
    OPEN_EMITTER = "open_emitter"
    NO_CONNECT = "no_connect"


class PinGraphicStyle(BarewordEnum):
    LINE = "line"
    INVERTED = "inverted"
    CLOCK = "clock"
    INVERTED_CLOCK = "inverted_clock"
    INPUT_LOW = "input_low"
    CLOCK_LOW = "clock_low"
    OUTPUT_LOW = "output_low"
    EDGE_CLOCK_HIGH = "edge_clock_high"
    NON_LOGIC = "non_logic"


@t.final
class Pin(Structure, car="pin"):
    class Alternate(Structure, car="alternate"):
        __slots__ = ("name", "electrical_type", "graphic_style")
        name: t.Final[str]
        electrical_type: t.Final[PinElectricalType]
        graphic_style: t.Final[PinGraphicStyle]

        def __init__(
            self,
            name: str,
            *,
            electrical_type: PinElectricalType = PinElectricalType.UNSPECIFIED,
            graphic_style: PinGraphicStyle = PinGraphicStyle.LINE,
        ):
            if isinstance(electrical_type, str):
                electrical_type = PinElectricalType(electrical_type)
            if isinstance(graphic_style, str):
                graphic_style = PinGraphicStyle(graphic_style)
            self.name = name
            self.electrical_type = electrical_type
            self.graphic_style = graphic_style
            super().__init__()

        def copy(self) -> Self:
            return self.__class__(
                self.name, electrical_type=self.electrical_type, graphic_style=self.graphic_style
            )

        def iter_children(self):
            return ()

        @classmethod
        def from_list_data(cls, data: list[KiValue]) -> Self:
            name = pop_value(data, str)
            electrical_type = PinElectricalType(pop_value(data, Bareword))
            graphic_style = PinGraphicStyle(pop_value(data, Bareword))

            return cls(name, electrical_type=electrical_type, graphic_style=graphic_style)

        @property
        def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...]]:
            yield self.name
            yield self.electrical_type
            yield self.graphic_style

    __slots__ = (
        "electrical_type",
        "graphic_style",
        "at",
        "length",
        "name",
        "name_effects",
        "number",
        "number_effects",
        "hide",
        "alternate",
    )
    electrical_type: t.Final[PinElectricalType]
    graphic_style: t.Final[PinGraphicStyle]
    at: t.Final[At]
    length: t.Final[float]
    name: t.Final[str]
    name_effects: t.Final[Effects]
    number: t.Final[str]  # sic!
    number_effects: t.Final[Effects]
    hide: t.Final[bool]
    alternate: t.Final[Alternate | None]

    _HIDE = Bareword("hide")

    def __init__(
        self,
        name: str,
        number: str | int,
        *,
        at: At,
        length: float = 2.54,
        electrical_type: str | PinElectricalType = PinElectricalType.UNSPECIFIED,
        graphic_style: str | PinGraphicStyle = PinGraphicStyle.LINE,
        hide: bool = False,
        name_effects: Effects | None = None,
        number_effects: Effects | None = None,
        alternate: Alternate | None = None,
    ):
        if isinstance(electrical_type, str):
            electrical_type = PinElectricalType(electrical_type)
        if isinstance(graphic_style, str):
            graphic_style = PinGraphicStyle(graphic_style)
        self.name = name
        self.name_effects = name_effects or Effects()
        self.number = str(number)
        self.number_effects = number_effects or Effects()
        self.at = at
        self.length = length
        self.electrical_type = electrical_type
        self.graphic_style = graphic_style
        self.hide = hide
        self.alternate = alternate
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.name,
            self.number,
            at=self.at.copy(),
            length=self.length,
            electrical_type=self.electrical_type,
            graphic_style=self.graphic_style,
            hide=self.hide,
            name_effects=self.name_effects.copy(),
            number_effects=self.number_effects.copy(),
            alternate=self.alternate and self.alternate.copy(),
        )

    def iter_children(self):
        yield self.at
        yield self.name_effects
        yield self.number_effects

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        def _extract_pin_nn(data: list[KiValue], key: str) -> tuple[str, Effects]:
            (nn_l,) = List.extract_from(data, key)
            (nn, fx_l) = nn_l.cdr
            assert isinstance(nn, str)
            assert isinstance(fx_l, List)
            fx = Effects.from_list(fx_l)
            return nn, fx

        electrical_type = PinElectricalType(pop_value(data, Bareword))
        graphic_style = PinGraphicStyle(pop_value(data, Bareword))
        (at,) = At.extract_from(data)
        length = must(extract_value(data, "length", float))
        name, name_effects = _extract_pin_nn(data, "name")
        number, number_effects = _extract_pin_nn(data, "number")
        hide = extract_bareword_flag(data, cls._HIDE)
        alternate = only(Pin.Alternate.extract_from(data))

        return cls(
            name,
            number,
            at=at,
            length=length,
            electrical_type=electrical_type,
            graphic_style=graphic_style,
            hide=hide,
            name_effects=name_effects,
            number_effects=number_effects,
            alternate=alternate,
        )

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...] | None]:
        yield self.electrical_type
        yield self.graphic_style
        yield self.at
        yield "length", self.length
        if self.hide:
            yield self._HIDE
        yield "name", self.name, self.name_effects
        yield "number", self.number, self.number_effects
        yield self.alternate

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.number
        yield self.name
        yield "electrical_type", self.electrical_type.value
        yield "graphic_style", self.graphic_style.value
        yield "length", self.length
        yield "at", self.at
        yield "hide", self.hide, False
        yield "name_effects", self.name_effects
        yield "number_effects", self.number_effects
