from __future__ import annotations

import typing as t

from ..._util import Self
from ...bareword import Bareword
from ...list import List
from ...typing import KiValue
from .._container import ContainedStructure
from .._helpers import extract_cdr, extract_value, only, pop_value
from ..property import Property
from ..symbol_properties import SymbolProperties
from .pin_names import PinNames
from .pin_numbers import PinNumbers
from .symbol_unit import SymbolUnit


class SymbolBase(ContainedStructure[str]):
    """A (symbol) in a .kicad_sym file"""

    __slots__ = (
        "name",
        "power",
        "pin_numbers",
        "pin_names",
        "in_bom",
        "on_board",
        "properties",
        "units",
    )
    name: str
    power: bool
    pin_numbers: PinNumbers | None
    pin_names: PinNames | None
    in_bom: bool | None
    on_board: bool | None
    properties: SymbolProperties
    units: dict[tuple[int, int], SymbolUnit]

    def __init__(
        self,
        name: str,
        *,
        power: bool = False,
        pin_numbers: PinNumbers | None = None,
        pin_names: PinNames | None = None,
        in_bom: bool | None = True,
        on_board: bool | None = True,
        properties: t.Iterable[Property] = (),
        units: t.Iterable[SymbolUnit] = (),
    ):
        self.name = name
        self.power = power
        self.pin_numbers = pin_numbers
        self.pin_names = pin_names
        self.in_bom = in_bom
        self.on_board = on_board
        self.properties = SymbolProperties(properties, parent=self)
        self.units = {(u.unit, u.style): u for u in units}
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.name,
            power=self.power,
            pin_numbers=self.pin_numbers and self.pin_numbers.copy(),
            pin_names=self.pin_names and self.pin_names.copy(),
            in_bom=self.in_bom,
            on_board=self.on_board,
            properties=self.properties.copy_values(),
            units=(u.copy() for u in self.units.values()),
        )

    __copy__ = copy

    def iter_children(self):
        if self.pin_numbers:
            yield self.pin_numbers
        if self.pin_names:
            yield self.pin_names
        yield from self.units.values()

    _HIDE: t.Final = Bareword("hide")

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        name = pop_value(data, str)
        power = extract_cdr(data, "power") is not None
        pin_numbers = only(PinNumbers.extract_from(data))
        pin_names = only(PinNames.extract_from(data))
        in_bom = extract_value(data, "in_bom", bool)
        on_board = extract_value(data, "on_board", bool)
        properties = Property.extract_from(data)
        units = SymbolUnit.extract_from(data)

        return cls(
            name,
            power=power,
            pin_numbers=pin_numbers,
            pin_names=pin_names,
            in_bom=in_bom,
            on_board=on_board,
            properties=properties,
            units=units,
        )

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        yield self.name
        if self.power:
            yield "power",
        yield self.pin_numbers
        yield self.pin_names
        if self.in_bom is not None:
            yield "in_bom", self.in_bom
        if self.on_board is not None:
            yield "on_board", self.on_board
        yield from self.properties.iter_values()
        yield from self.units.values()

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.name
        yield "power", self.power, False
        yield "pin_numbers", self.pin_numbers, None
        yield "pin_names", self.pin_names, None
        yield "in_bom", self.in_bom
        yield "on_board", self.on_board
        yield "properties", self.properties
        yield "units", self.units

    @classmethod
    def container__key(cls, item: Self | List) -> str:
        if isinstance(item, SymbolBase):
            return item.name
        name = item.cdr[0]
        assert isinstance(name, str)
        return name
