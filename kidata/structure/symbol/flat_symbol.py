from __future__ import annotations

import typing as t
from functools import cache

from ..at import At
from ..graphical import Graphical
from ..property import Property
from ..symbol_properties import SymbolProperties
from .pin import Pin
from .pin_names import PinNames
from .pin_numbers import PinNumbers
from .symbol_base import SymbolBase
from .symbol_unit import SymbolUnit


class UnitData:
    __slots__ = (
        "lib_id",
        "unit",
        "convert",
        "pin_numbers",
        "pin_names",
        "in_bom",
        "on_board",
        "properties",
        "graphical",
        "pins",
    )
    lib_id: t.Final[str]
    unit: t.Final[int]
    convert: t.Final[int]
    pin_numbers: t.Final[PinNumbers | None]
    pin_names: t.Final[PinNames | None]
    in_bom: t.Final[bool]
    on_board: t.Final[bool]
    properties: t.Final[SymbolProperties]
    graphical: t.Final[t.Sequence[Graphical]]
    pins: t.Final[t.Mapping[str, Pin]]

    def __init__(
        self,
        sym: FlatSymbol,
        unit: int = 1,
        convert: int = 1,
    ):
        self.lib_id = sym.name
        self.unit = unit
        self.convert = convert
        self.pin_numbers = sym.pin_numbers and sym.pin_numbers.copy()
        self.pin_names = sym.pin_names and sym.pin_names.copy()
        self.in_bom = sym.in_bom
        self.on_board = sym.on_board
        self.properties = SymbolProperties(sym.properties.copy_values())
        self.graphical = []
        self.pins = {}

        found_unit = False
        for unit_key in [(0, 0), (0, convert), (unit, 0), (unit, convert)]:
            if sym_unit := sym.units.get(unit_key):
                found_unit = True
                self.graphical.extend(gr.copy() for gr in sym_unit.graphical)
                self.pins.update((k, v.copy()) for k, v in sym_unit.pins.items())
        assert found_unit

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.lib_id
        yield "unit", self.unit
        yield "convert", self.convert, 1
        yield "pin_numbers", self.pin_numbers, None
        yield "pin_names", self.pin_names, None
        yield "in_bom", self.in_bom
        yield "on_board", self.on_board
        yield "properties", self.properties
        yield "graphical", self.graphical
        yield "pins", self.pins

    def pin_at(self, pin: str) -> At:
        return self.pins[pin].at.mirror_y


@t.final
class FlatSymbol(SymbolBase, car="symbol"):
    """A (symbol) in (lib_symbols) section of .kicad_sch file"""

    __slots__ = ()
    in_bom: bool
    on_board: bool

    def __init__(
        self,
        name: str,
        *,
        power: bool = False,
        pin_numbers: PinNumbers | None = None,
        pin_names: PinNames | None = None,
        in_bom: bool = True,
        on_board: bool = True,
        properties: t.Iterable[Property] = (),
        units: t.Iterable[SymbolUnit] = (),
    ):
        assert in_bom is not None
        assert on_board is not None

        super().__init__(
            name,
            power=power,
            pin_numbers=pin_numbers,
            pin_names=pin_names,
            in_bom=in_bom,
            on_board=on_board,
            properties=properties,
            units=units,
        )

    @property
    def n_units(self) -> int:
        return max(k[0] for k in self.units)

    @cache
    def unit(self, unit: int = 1, convert: int = 1) -> UnitData:
        return UnitData(self, unit, convert)
