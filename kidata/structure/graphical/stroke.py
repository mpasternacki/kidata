from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...typing import KiValue
from .._helpers import BarewordEnum, extract_value, must, only
from ..structure import Structure
from .color import Color


@t.final
class Stroke(Structure, car="stroke"):
    class Type(BarewordEnum):
        DASH = "dash"
        DASH_DOT = "dash_dot"
        DEFAULT = "default"
        DOT = "dot"
        SOLID = "solid"

    __slots__ = ("width", "type", "color")
    width: t.Final[float]
    type: t.Final[Type | None]
    color: t.Final[Color | None]

    def __init__(
        self,
        width: float = 0,
        type_: Type | None = Type.DEFAULT,
        color: Color | None | t.Literal[DEFAULT] = DEFAULT,
    ):
        self.width = width
        self.type = type_
        self.color = Color.default() if color is DEFAULT else color
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self.width, self.type, self.color.copy() if self.color else None)

    __copy__ = copy

    def iter_children(self):
        if self.color:
            yield self.color

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Stroke:
        width = must(extract_value(data, "width", float))
        type_ = Stroke.Type.extract_attribute(data, "type")
        color = only(Color.extract_from(data))
        return cls(width, type_, color)

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...] | None]:
        yield "width", self.width
        if self.type:
            yield "type", self.type
        yield self.color

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield "width", self.width, 0
        yield "type_", self.type, Stroke.Type.DEFAULT
        yield "color", self.color

    def __eq__(self, other: t.Any) -> bool:
        if isinstance(other, Stroke):
            return (self.width, self.type, self.color) == (other.width, other.type, other.color)
        return NotImplemented
