from __future__ import annotations

import typing as t

from ..._util import Self
from ...typing import KiValue
from .._helpers import pop_value
from ..structure import Structure


@t.final
class Color(Structure, car="color"):
    __slots__ = ("r", "g", "b", "a")
    r: t.Final[float]
    g: t.Final[float]
    b: t.Final[float]
    a: t.Final[float]

    def __init__(self, r: float, g: float, b: float, a: float = 0):
        self.r = r
        self.g = g
        self.b = b
        self.a = a
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self.r, self.g, self.b, self.a)

    __copy__ = copy

    @classmethod
    def default(cls):
        return cls(0, 0, 0, 0)

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        r = pop_value(data, float)
        g = pop_value(data, float)
        b = pop_value(data, float)
        a = pop_value(data, float)
        return cls(r, g, b, a)

    @property
    def cdr(self) -> t.Iterator[KiValue]:
        yield self.r
        yield self.g
        yield self.b
        yield self.a

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.r
        yield self.g
        yield self.b
        yield None, self.a, 0

    def __eq__(self, other: t.Any) -> bool:
        if isinstance(other, Color):
            return (self.r, self.g, self.b, self.a) == (other.r, other.b, other.g, other.a)
        return NotImplemented
