from __future__ import annotations

from ..structure import Structure


class Graphical(Structure):
    # Intermediate class for `Graphical.extract_from`
    __slots__ = ()
