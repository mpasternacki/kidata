from __future__ import annotations

import typing as t

from ..._util import Self
from ...bareword import Bareword
from ...list import List
from ...typing import KiValue
from .._helpers import extract_bareword_flag, extract_value
from ..structure import Structure


@t.final
class Font(Structure, car="font"):
    __slots__ = ("size", "thickness", "bold", "italic")
    size: t.Final[tuple[float, float]]
    thickness: t.Final[float | None]
    bold: t.Final[bool]
    italic: t.Final[bool]

    _BOLD: t.Final = Bareword("bold")
    _ITALIC: t.Final = Bareword("italic")

    def __init__(
        self,
        size: t.SupportsFloat | tuple[t.SupportsFloat, t.SupportsFloat] = 1.27,
        *,
        thickness: t.SupportsFloat | None = None,
        bold: bool = False,
        italic: bool = False,
    ):
        if isinstance(size, tuple):
            the_size = (float(size[0]), float(size[1]))
        else:
            size = float(size)
            the_size = (size, size)
        self.size = the_size
        self.thickness = float(thickness) if thickness is not None else None
        self.bold = bold
        self.italic = italic
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.size, thickness=self.thickness, bold=self.bold, italic=self.italic
        )

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (size,) = List.extract_from(data, "size")
        (height, width) = size.cdr
        assert isinstance(height, (int, float))
        assert isinstance(width, (int, float))

        thickness = extract_value(data, "thickness", float)
        bold = extract_bareword_flag(data, "bold")
        italic = extract_bareword_flag(data, "italic")

        return cls(size=(width, height), thickness=thickness, bold=bold, italic=italic)

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...]]:
        yield "size", *self.size
        if self.thickness is not None:
            yield "thickness", self.thickness
        if self.bold:
            yield self._BOLD
        if self.italic:
            yield self._ITALIC

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        if self.size != (1.27, 1.27):
            yield "size", self.size
        yield "thickness", self.thickness, None
        yield "bold", self.bold, False
        yield "italic", self.italic, False

    def __eq__(self, other: t.Any) -> bool:
        if isinstance(other, Font):
            return (self.size, self.thickness, self.bold, self.italic) == (
                other.size,
                other.thickness,
                other.bold,
                other.italic,
            )
        return NotImplemented
