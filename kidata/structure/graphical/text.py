from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...typing import KiValue
from .._helpers import only, pop_value
from ..at import At
from ..uuid import UUID
from .base import Graphical
from .effects import Effects


@t.final
class Text(Graphical, car="text"):
    __slots__ = ("text", "at", "effects", "uuid")
    text: str
    at: At
    effects: Effects
    uuid: UUID | None

    def __init__(
        self,
        text: str,
        at: At,
        *,
        effects: Effects | t.Literal[DEFAULT] = DEFAULT,
        uuid: UUID | None = None,
    ):
        self.text = text
        self.at = at
        self.effects = effects if effects is not DEFAULT else Effects()
        self.uuid = uuid
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.text,
            self.at.copy(),
            effects=self.effects.copy(),
            uuid=self.uuid.copy() if self.uuid else None,
        )

    __copy__ = copy

    def iter_children(self):
        yield self.at
        yield self.effects
        if self.uuid:
            yield self.uuid

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        text = pop_value(data, str)
        (at,) = At.extract_from(data)
        (effects,) = Effects.extract_from(data)
        uuid = only(UUID.extract_from(data))
        return cls(text, at, effects=effects, uuid=uuid)

    @property
    def cdr(self) -> t.Iterator[KiValue]:
        yield self.text
        yield self.at
        yield self.effects
        if self.uuid is not None:
            yield self.uuid

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.text
        yield self.at
        yield "effects", self.effects
        yield "uuid", self.uuid, None
