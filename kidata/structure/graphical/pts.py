from __future__ import annotations

import sys
import typing as t

from ..._util import Self
from ...list import List
from ...typing import KiValue
from ..structure import Structure

_XY = sys.intern("xy")


@t.final
class Pts(Structure, car="pts"):
    __slots__ = ("points",)
    points: tuple[tuple[float, float], ...]

    def __init__(self, points: tuple[tuple[float, float], ...]):
        self.points = points
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self.points)

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        def _gen_points() -> t.Iterator[tuple[float, float]]:
            for item in data:
                assert isinstance(item, List)
                assert item.car is _XY
                (x, y) = item.cdr
                assert isinstance(x, (int, float))
                assert isinstance(y, (int, float))
                yield (x, y)

        points = tuple(_gen_points())
        data.clear()
        return cls(points)

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...]]:
        for pt in self.points:
            yield "xy", *pt

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield None, self.points
