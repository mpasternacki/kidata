from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...typing import KiValue
from .._helpers import only
from ..uuid import UUID
from .base import Graphical
from .fill import Fill
from .pts import Pts
from .stroke import Stroke


@t.final
class Polyline(Graphical, car="polyline"):
    __slots__ = ("pts", "stroke", "fill", "uuid")
    pts: t.Final[Pts]
    stroke: t.Final[Stroke]
    fill: t.Final[Fill | None]
    uuid: t.Final[UUID | None]

    def __init__(
        self,
        pts: Pts,
        *,
        stroke: Stroke | t.Literal[DEFAULT] = DEFAULT,
        fill: Fill | None | t.Literal[DEFAULT] = DEFAULT,
        uuid: UUID | None = None,
    ):
        self.pts = pts
        self.stroke = stroke if stroke is not DEFAULT else Stroke()
        self.fill = Fill() if fill is DEFAULT else fill
        self.uuid = uuid
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.pts.copy(),
            stroke=self.stroke.copy(),
            fill=self.fill.copy() if self.fill else None,
            uuid=self.uuid.copy() if self.uuid else None,
        )

    def iter_children(self):
        yield self.pts
        yield self.stroke
        if self.fill:
            yield self.fill
        if self.uuid:
            yield self.uuid

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (pts,) = Pts.extract_from(data)
        (stroke,) = Stroke.extract_from(data)
        fill = only(Fill.extract_from(data))
        uuid = only(UUID.extract_from(data))
        return cls(pts, stroke=stroke, fill=fill, uuid=uuid)

    @property
    def cdr(self) -> t.Iterator[KiValue | None]:
        yield self.pts
        yield self.stroke
        yield self.fill
        yield self.uuid

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield None, self.pts
        yield "stroke", self.stroke
        yield "fill", self.fill
        yield "uuid", self.uuid, None
