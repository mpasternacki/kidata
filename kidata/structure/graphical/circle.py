from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...typing import KiValue
from .._helpers import extract_coords, extract_value, must, only
from .base import Graphical
from .fill import Fill
from .stroke import Stroke


@t.final
class Circle(Graphical, car="circle"):
    __slots__ = ("center", "radius", "stroke", "fill")
    center: t.Final[tuple[float, float]]
    radius: t.Final[float]
    stroke: t.Final[Stroke | None]
    fill: t.Final[Fill]

    def __init__(
        self,
        center: tuple[float, float],
        radius: float,
        *,
        stroke: Stroke | None | t.Literal[DEFAULT] = DEFAULT,
        fill: Fill | t.Literal[DEFAULT] = DEFAULT,
    ):
        self.center = center
        self.radius = radius
        self.stroke = Stroke() if stroke is DEFAULT else stroke
        self.fill = fill if fill is not DEFAULT else Fill()
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.center,
            self.radius,
            stroke=self.stroke.copy() if self.stroke else None,
            fill=self.fill.copy(),
        )

    __copy__ = copy

    def iter_children(self):
        if self.stroke:
            yield self.stroke
        yield self.fill

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        center = extract_coords(data, "center")
        radius = must(extract_value(data, "radius", float))
        stroke = only(Stroke.extract_from(data), discard=True)  # FIXME: kicad library
        (fill,) = Fill.extract_from(data)
        return cls(center, radius, stroke=stroke, fill=fill)

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...]]:
        yield "center", *self.center
        yield "radius", self.radius
        if self.stroke is not None:
            yield self.stroke
        yield self.fill

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield None, self.center
        yield self.radius
        yield "stroke", self.stroke
        yield "fill", self.fill
