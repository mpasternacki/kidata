from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...typing import KiValue
from .._helpers import extract_coords
from .base import Graphical
from .fill import Fill
from .stroke import Stroke


@t.final
class Arc(Graphical, car="arc"):
    __slots__ = ("start", "mid", "end", "stroke", "fill")
    start: t.Final[tuple[float, float]]
    mid: t.Final[tuple[float, float]]
    end: t.Final[tuple[float, float]]
    stroke: t.Final[Stroke]
    fill: t.Final[Fill]

    def __init__(
        self,
        start: tuple[float, float],
        mid: tuple[float, float],
        end: tuple[float, float],
        *,
        stroke: Stroke | t.Literal[DEFAULT] = DEFAULT,
        fill: Fill | t.Literal[DEFAULT] = DEFAULT,
    ):
        self.start = start
        self.mid = mid
        self.end = end
        self.stroke = stroke if stroke is not DEFAULT else Stroke()
        self.fill = fill if fill is not DEFAULT else Fill()
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.start, self.mid, self.end, stroke=self.stroke.copy(), fill=self.fill.copy()
        )

    __copy__ = copy

    def iter_children(self):
        yield self.stroke
        yield self.fill

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        start = extract_coords(data, "start")
        mid = extract_coords(data, "mid")
        end = extract_coords(data, "end")
        (stroke,) = Stroke.extract_from(data)
        (fill,) = Fill.extract_from(data)
        return cls(start, mid, end, stroke=stroke, fill=fill)

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...]]:
        yield "start", *self.start
        yield "mid", *self.mid
        yield "end", *self.end
        yield self.stroke
        yield self.fill

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield None, self.start
        yield None, self.mid
        yield None, self.end
        yield "stroke", self.stroke
        yield "fill", self.fill
