from __future__ import annotations

import codecs
import typing as t

from ..._util import Self
from ...api import DEFAULT, CantHappen
from ...bareword import Bareword
from ...typing import KiValue
from .._helpers import extract_value
from ..at import At
from ..structure import Structure
from ..uuid import UUID
from .base import Graphical


@t.final
class Image(Graphical, car="image"):
    @t.final
    class Data(Structure, car="data"):
        __slots__ = ("data",)
        data: bytes

        def __init__(self, data: bytes):
            self.data = data
            super().__init__()

        def copy(self) -> Self:
            return self.__class__(self.data)

        __copy__ = copy

        def iter_children(self):
            return ()

        @classmethod
        def from_list_data(cls, data: list[KiValue]) -> Self:
            def _data_lines() -> t.Iterator[bytes]:
                for v in data:
                    assert isinstance(v, Bareword)
                    yield v.value.encode("us-ascii")

            data_bytes = b"\n".join(_data_lines())
            data.clear()
            return cls(codecs.decode(data_bytes, "base64"))

        @property
        def cdr(self) -> t.NoReturn:
            """Stub to fulfill ABC"""
            raise CantHappen()

        def _kiformat_(self, f: t.TextIO) -> None:
            f.write("(data\n")
            f.write(codecs.encode(self.data, "base64").decode("us-ascii"))
            f.write(")")

        def __rich_repr__(self) -> t.Iterator[t.Any]:
            yield len(self.data)

        __rich_repr__.angular = True  # type: ignore

    __slots__ = ("at", "scale", "uuid", "data")
    at: At
    scale: float
    uuid: UUID
    data: Data

    def __init__(
        self,
        data: bytes | Data,
        at: At,
        *,
        scale: float = 1,
        uuid: UUID | t.Literal[DEFAULT] = DEFAULT,
    ):
        if not isinstance(data, Image.Data):
            data = Image.Data(data)
        self.at = at
        self.scale = scale
        self.uuid = uuid if uuid is not DEFAULT else UUID()
        self.data = data
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.data.copy(), self.at.copy(), scale=self.scale, uuid=self.uuid.copy()
        )

    __copy__ = copy

    def iter_children(self):
        yield self.data
        yield self.at
        yield self.uuid

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (at,) = At.extract_from(data)
        scale = extract_value(data, "scale", float) or 1
        (uuid,) = UUID.extract_from(data)
        (the_data,) = Image.Data.extract_from(data)
        return cls(the_data, at, scale=scale, uuid=uuid)

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[str, KiValue]]:
        yield self.at  # .unangled?
        if self.scale != 1:
            yield "scale", self.scale
        yield self.uuid
        yield self.data
