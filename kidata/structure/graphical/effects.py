from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...bareword import Bareword
from ...list import List
from ...typing import KiValue
from .._helpers import extract_bareword_flag
from ..structure import Structure
from .font import Font


@t.final
class Effects(Structure, car="effects"):
    __slots__ = ("font", "justify", "hide")
    font: t.Final[Font]
    justify: t.Final[tuple[Bareword, ...] | None]
    hide: t.Final[bool]

    _HIDE: t.Final = Bareword("hide")

    def __init__(
        self,
        font: Font | t.Literal[DEFAULT] = DEFAULT,
        justify: t.Sequence[Bareword | str] | Bareword | str | None = None,
        hide: bool = False,
    ):
        the_justify: tuple[Bareword, ...] | None = None
        if justify is None:
            pass
        elif isinstance(justify, Bareword):
            the_justify = (justify,)
        elif isinstance(justify, str):
            the_justify = (Bareword(justify),)
        else:
            the_justify = tuple(j if isinstance(j, Bareword) else Bareword(j) for j in justify)
        self.font = font if font is not DEFAULT else Font()
        self.justify = the_justify
        self.hide = hide
        super().__init__()

    def copy(
        self,
        *,
        font: Font | t.Literal[DEFAULT] = DEFAULT,
        justify: t.Sequence[Bareword | str] | Bareword | str | None | t.Literal[DEFAULT] = DEFAULT,
        hide: bool | t.Literal[DEFAULT] = DEFAULT,
    ) -> Self:
        return self.__class__(
            font if font is not DEFAULT else self.font.copy(),
            justify if justify is not DEFAULT else self.justify,
            hide=hide if hide is not DEFAULT else self.hide,
        )

    __copy__ = copy

    def iter_children(self):
        yield self.font

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (font,) = Font.extract_from(data)

        justify: tuple[Bareword, ...] | None = None
        if justify_ll := tuple(List.extract_from(data, "justify")):
            (justify_l,) = justify_ll

            def _gen_justify() -> t.Iterator[Bareword]:
                for v in justify_l.cdr:
                    if isinstance(v, Bareword):
                        yield v
                    else:
                        raise ValueError(f"Not a Bareword in (justify ...): {v!r})")

            justify = tuple(_gen_justify())

        hide = extract_bareword_flag(data, "hide")

        return cls(font=font, justify=justify, hide=hide)

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...]]:
        yield self.font
        if self.justify is not None:
            yield "justify", *self.justify
        if self.hide:
            yield self._HIDE

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield "font", self.font
        yield "justify", self.justify, None
        yield "hide", self.hide, False

    def __eq__(self, other: t.Any) -> bool:
        if isinstance(other, Effects):
            return (self.font, self.justify, self.hide) == (
                other.font,
                other.justify,
                other.hide,
            )
        return NotImplemented
