from __future__ import annotations

import typing as t

from ..._util import Self
from ...typing import KiValue
from .._helpers import BarewordEnum, must
from ..structure import Structure


@t.final
class Fill(Structure, car="fill"):
    class Type(BarewordEnum):
        NONE = "none"
        SOLID = "solid"
        BACKGROUND = "background"  # FIXME: not valid in every context
        OUTLINE = "outline"  # not documented

    __slots__ = ("type",)
    type: t.Final[Type]

    def __init__(self, type_: Type = Type.NONE):
        self.type = type_
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self.type)

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        type_ = must(Fill.Type.extract_attribute(data, "type"))
        return cls(type_)

    @property
    def cdr(self):
        yield "type", self.type

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.type.value

    def __eq__(self, other: t.Any) -> bool:
        if isinstance(other, Fill):
            return self.type == other.type
        return NotImplemented
