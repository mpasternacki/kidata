from __future__ import annotations

import typing as t

from ..._util import Self
from ...api import DEFAULT
from ...typing import KiValue
from .._helpers import extract_coords
from .base import Graphical
from .fill import Fill
from .stroke import Stroke


@t.final
class Rectangle(Graphical, car="rectangle"):
    __slots__ = ("start", "end", "stroke", "fill")
    start: t.Final[tuple[float, float]]
    end: t.Final[tuple[float, float]]
    stroke: t.Final[Stroke]
    fill: t.Final[Fill]

    def __init__(
        self,
        start: tuple[float, float],
        end: tuple[float, float],
        *,
        stroke: Stroke | t.Literal[DEFAULT] = DEFAULT,
        fill: Fill | t.Literal[DEFAULT] = DEFAULT,
    ):
        self.start = start
        self.end = end
        self.stroke = stroke if stroke is not DEFAULT else Stroke()
        self.fill = fill if fill is not DEFAULT else Fill()
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self.start, self.end, stroke=self.stroke.copy(), fill=self.fill.copy()
        )

    __copy__ = copy

    def iter_children(self):
        yield self.stroke
        yield self.fill

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        start = extract_coords(data, "start")
        end = extract_coords(data, "end")
        (stroke,) = Stroke.extract_from(data)
        (fill,) = Fill.extract_from(data)
        return cls(start, end, stroke=stroke, fill=fill)

    @property
    def cdr(self) -> t.Iterator[KiValue | tuple[KiValue, ...]]:
        yield "start", *self.start
        yield "end", *self.end
        yield self.stroke
        yield self.fill

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield None, self.start
        yield None, self.end
        yield "stroke", self.stroke
        yield "fill", self.fill
