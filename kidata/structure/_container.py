from __future__ import annotations

import abc
import collections.abc
import types
import typing as t
from itertools import count
from warnings import warn

from .._util import Self, consume
from ..list import List
from ..typing import KiValue
from .structure import Structure

T_Key = t.TypeVar("T_Key", bound=t.Hashable)
T_UnloadedItem = t.TypeVar("T_UnloadedItem")
T_Item = t.TypeVar("T_Item", bound="Contained[t.Any, t.Any]", covariant=True)


class Contained(Structure, t.Generic[T_Key, T_UnloadedItem]):
    __slots__ = ()

    @classmethod
    def __init_subclass__container_classvar(cls) -> None:
        if (container := cls.__dict__.get("Container")) is None:
            # No Container property defined directly on class
            return
        if (origin := getattr(container, "__origin__", None)) is None:
            # Container is not a GenericAlias
            return
        if not (
            isinstance(origin, type)
            and issubclass(origin, Container)
            and getattr(container, "__parameters__", None) == ()
        ):
            # Container is either not fully bound (like in ContainedStructure),
            # or alias doesn't point at Structure subclass
            return

        (_, item_cls, _) = t.cast(tuple[t.Any, ...], getattr(container, "__args__", ()))
        if item_cls is not Self:
            raise TypeError(
                f"Second arg of {cls}.Container ({container}) is {item_cls}, not {Self}"
            )
            return

        def _exec_body(ns: dict[str, t.Any]) -> None:
            ns["_Container__item_cls"] = cls
            ns["__module__"] = cls.__module__
            ns["__orig_generic_alias__"] = container
            ns["__slots__"] = ()

        cls.Container = types.new_class("Container", (container,), exec_body=_exec_body)
        cls.Container.__qualname__ = f"{cls.__qualname__}.Container"

    def __init_subclass__(cls, **kwargs: t.Any) -> None:
        cls.__init_subclass__container_classvar()
        super().__init_subclass__(**kwargs)

    class DuplicateKeyError(ValueError):
        pass

    @classmethod
    @abc.abstractmethod
    def container__key(cls, item: Self | T_UnloadedItem) -> T_Key:
        raise NotImplementedError()

    @classmethod
    @abc.abstractmethod
    def container__load(
        cls, item: T_UnloadedItem, container: Container[T_Key, Self, T_UnloadedItem]
    ) -> Self:
        raise NotImplementedError()

    @classmethod
    def container__check_unloaded(cls, item: T_UnloadedItem) -> None:
        """Override to add own asserts/requirements/compatibility checks"""
        pass

    @classmethod
    def container__deduplicate_key(cls, key: T_Key, attempt: int) -> T_Key:
        raise ContainedStructure.DuplicateKeyError(f"Duplicate key {key!r} of {cls.__qualname__}")


class DuplicateWarning(UserWarning):
    pass


class Container(t.Generic[T_Key, T_Item, T_UnloadedItem], collections.abc.Mapping[T_Key, T_Item]):
    __slots__ = ("_items", "_map", "_parent")
    __item_cls: type[T_Item]
    _items: list[T_Item | T_UnloadedItem | None]
    _map: dict[T_Key, int]
    _parent: Structure | None

    def __init__(
        self: Container[T_Key, T_Item, T_UnloadedItem],
        data: t.Iterable[T_Item | T_UnloadedItem] = (),
        *,
        parent: Structure | None = None,
    ):
        cls = self.__item_cls
        self._items = list(data)
        self._map = {}
        self._parent = parent

        for i in range(len(self._items)):
            item = self._items[i]
            if not isinstance(item, self.__item_cls):
                self.__item_cls.container__check_unloaded(item)
            # TODO: else set parent?
            name = self.__item_cls.container__key(item)
            if name in self._map:
                for attempt in count():
                    name1 = self.__item_cls.container__deduplicate_key(name, attempt)
                    if name1 not in self._map:
                        warn(
                            DuplicateWarning(
                                f"Duplicate {cls.__qualname__} key {name!r}, indexing as {name1!r}"
                            )
                        )
                        name = name1
                        break
            self._map[name] = i

    def copy_items(self) -> t.Iterator[T_Item | T_UnloadedItem]:
        for item in self._items:
            if item is not None:
                yield item.copy() if isinstance(item, self.__item_cls) else item

    def copy(self) -> Self:
        return self.__class__(self.copy_items())

    __copy__ = copy

    def iter_children(self) -> t.Iterator[T_Item]:
        for obj in self._items:
            if isinstance(obj, self.__item_cls):
                yield obj

    def iter_lazy(self) -> t.Iterator[tuple[T_Key, T_Item | T_UnloadedItem]]:
        for k, i in self._map.items():
            v = self._items[i]
            if v is not None:
                yield k, v

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        for k, v in self._map.items():
            yield str(k), self._items[v]

    def __len__(self) -> int:
        return len(self._map)

    def __getitem__(self: Container[T_Key, T_Item, T_UnloadedItem], k: T_Key) -> T_Item:
        i = self._map[k]
        obj = self._items[i]

        if obj is None:
            raise KeyError(k)

        if isinstance(obj, self.__item_cls):
            return obj

        obj2 = self.__item_cls.container__load(obj, container=self)
        if self._parent is not None:
            obj2.parent = self._parent
        self._items[i] = obj2
        return obj2

    def __iter__(self) -> t.Iterator[T_Key]:
        return iter(self._map)

    def __contains__(self, k: object) -> bool:
        return k in self._map

    def cdr_values(self) -> t.Iterable[T_Item | T_UnloadedItem | None]:
        yield from self._items

    def unlazificate(self) -> None:
        for v in self.values():  # .values() implicitly loads each value
            v.unlazificate()

    def unlazificate1(self) -> None:
        consume(self.values())  # implicitly load each value and discard


T_Key2 = t.TypeVar("T_Key2", bound=t.Hashable)


class ContainedStructure(Contained[T_Key, List]):
    __slots__ = ("container",)

    container: Container[T_Key, Self, List]

    # Generic type alias within class cannot use bound type variables T_Key
    # Self seems to behave weird in subclasses too
    Container = Container[T_Key2, T_Item, List]

    @classmethod
    def extract_container(cls, data: list[KiValue]) -> Container[T_Key, Self, List]:
        return cls.Container(List.extract_from(data, cls.car))  # type: ignore

    @classmethod
    def container__check_unloaded(cls, item: List) -> None:
        assert item.car is cls.car

    @classmethod
    def container__load(cls, item: List, container: Container[T_Key, Self, List]) -> Self:
        rv = cls.from_list(item)
        rv.container = container
        return rv
