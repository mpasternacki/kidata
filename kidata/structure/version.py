from __future__ import annotations

import datetime
import typing as t

from .._util import Self
from ..typing import KiValue
from ._helpers import pop_value
from .structure import Structure

# Version is specified to be a date in YYYYMMDD format


@t.final
class Version(Structure, car="version"):
    DEFAULT_DATE = datetime.date(2021, 11, 23)
    __slots__ = ("date",)
    date: t.Final[datetime.date]

    def __init__(self, date: datetime.date | None = None):
        if date is None:
            date = self.DEFAULT_DATE
        self.date = date
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self.date)

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        ts = pop_value(data, int)

        yyyymmdd = f"{ts:08}"
        assert len(yyyymmdd) == 8
        yyyy = yyyymmdd[:4]
        mm = yyyymmdd[4:6]
        dd = yyyymmdd[6:]

        return cls(datetime.date.fromisoformat(f"{yyyy}-{mm}-{dd}"))

    @property
    def cdr(self) -> t.Iterator[KiValue]:
        yield int(self.date.strftime("%Y%m%d"))

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.date
