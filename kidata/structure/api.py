from __future__ import annotations

import typing as t


class _ExtraDataExceptionBase(BaseException):
    def __init__(self, context: t.Any, data: t.Sequence[t.Any]):
        assert len(data) > 0
        data_s = ", ".join(repr(datum) for datum in data)
        super().__init__(f"Extra data present: {context}: {data_s}")


class LeftoverDataWarning(_ExtraDataExceptionBase, UserWarning):
    pass


class LeftoverDataError(_ExtraDataExceptionBase, ValueError):
    pass
