from __future__ import annotations

import os
import typing as t
from pathlib import Path

from .._util import Self
from ..format import kiformat
from ..list import List
from ..parse import kiparse
from .structure import Structure


class ParseableStructure(Structure):
    # KIFORMAT_ITEM_SEPARATOR = "\n  "
    __BY_SUFFIX: t.Final[dict[str, type[Self]]] = {}

    __slots__ = ("path",)
    path: Path

    def __init_subclass__(cls, *, suffix: str | None = None, **kwargs: t.Any) -> None:
        if suffix is not None:
            ParseableStructure.__BY_SUFFIX[suffix] = cls
        super().__init_subclass__(**kwargs)

    @staticmethod
    def class_for_path(path: os.PathLike[t.Any]) -> type[ParseableStructure] | None:
        path = Path(path)
        return ParseableStructure.__BY_SUFFIX.get(path.suffix or path.name)

    @classmethod
    def from_bytes(cls, data: bytes) -> Self:
        (raw_list,) = kiparse(data)
        assert isinstance(raw_list, List)
        return cls.from_list(raw_list)

    @classmethod
    def from_str(cls, data: str) -> Self:
        return cls.from_bytes(data.encode())

    @classmethod
    def from_file(cls, path: os.PathLike[t.Any], create: bool = False) -> Self:
        path = Path(path)
        if create and not path.exists():
            rv = cls()
        else:
            try:
                rv = cls.from_bytes(path.read_bytes())
            except Exception as exc:
                raise RuntimeError(f"Could not parse {path}") from exc
        rv.path = path
        return rv

    def save(self, path: os.PathLike[t.Any] | None = None) -> None:
        path = self.path if path is None else Path(path)
        with path.open("w") as save_f:
            kiformat(self, save_f)
