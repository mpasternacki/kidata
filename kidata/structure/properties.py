from __future__ import annotations

import typing as t

from .._util import Self
from ..format import kiformat_list
from ..list import List
from ..typing import KiValue


class Properties(dict[str, str]):
    From = Self | t.Mapping[str, str] | t.Iterable[tuple[str, str]] | None
    __slots__ = ()

    @classmethod
    def from_(cls, arg: From) -> Self:
        if arg is None:
            return cls()
        if isinstance(arg, cls):
            return arg
        return cls(arg)

    @classmethod
    def extract_from(cls, data: list[KiValue]) -> Self:
        def _kv_gen() -> t.Iterator[tuple[str, str]]:
            for property in List.extract_from(data, "property"):
                (key, val) = property.cdr
                assert isinstance(key, str)
                assert isinstance(val, str)
                yield (key, val)

        return cls(_kv_gen())

    def _kiformat_(self, f: t.TextIO) -> None:
        for kv in self.items():
            kiformat_list("property", kv, f)

    def copy(self) -> Self:
        return self.__class__(self.items())

    __copy__ = copy

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({super().__repr__()})"

    __str__ = __repr__

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        for k, v in self.items():
            yield k, v
