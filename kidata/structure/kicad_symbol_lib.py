from __future__ import annotations

import typing as t

from .._util import ProxiedMappingMixin, Self
from ..api import DEFAULT
from ..list import List
from ..typing import KiValue
from .generator import Generator
from .lib import ContainedLib
from .symbol import Symbol
from .version import Version


@t.final
class KicadSymbolLib(
    ContainedLib, ProxiedMappingMixin[str, Symbol], car="kicad_symbol_lib", suffix=".kicad_sym"
):
    Container = ContainedLib.Container[Self]

    __slots__ = ("version", "generator", "_proxied_map")
    version: Version
    generator: Generator
    _proxied_map: t.Final[Symbol.Container]

    def __init__(
        self,
        symbols: t.Iterable[Symbol | List] = (),
        *,
        generator: Generator | str | t.Literal[DEFAULT] = DEFAULT,
        version: Version | t.Literal[DEFAULT] = DEFAULT,
    ):
        if generator is DEFAULT:
            generator = Generator()
        elif isinstance(generator, str):
            generator = Generator(generator)

        self.version = version if version is not DEFAULT else Version()
        self.generator = generator
        self._proxied_map = Symbol.Container(symbols, parent=self)
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(
            self._proxied_map.copy_items(),
            version=self.version.copy(),
            generator=self.generator.copy(),
        )

    def iter_children(self):
        yield self.version
        yield self.generator
        yield from self._proxied_map.iter_children()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        (version,) = Version.extract_from(data)
        (generator,) = Generator.extract_from(data)
        return cls(List.extract_from(data, Symbol.car), version=version, generator=generator)

    @property
    def cdr(self) -> t.Iterable[KiValue | None]:
        yield self.version
        yield self.generator
        yield from self._proxied_map.cdr_values()

    def unlazificate(self):
        self._proxied_map.unlazificate()
        super().unlazificate()
