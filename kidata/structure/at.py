from __future__ import annotations

import math
import typing as t

from .._util import Self
from ..api import DEFAULT
from ..typing import KiValue
from ._helpers import pop_value
from .structure import Structure


@t.final
class At(Structure, car="at"):
    __slots__ = ("x", "y", "angle")
    x: t.Final[float]
    y: t.Final[float]
    angle: t.Final[float | None]

    def __init__(
        self, x: t.SupportsFloat, y: t.SupportsFloat, angle: t.SupportsFloat | None = None
    ):
        self.x = float(x)
        self.y = float(y)
        self.angle = float(angle) % 360 if angle is not None else None
        super().__init__()

    def copy(
        self,
        *,
        x: t.SupportsFloat | t.Literal[DEFAULT] = DEFAULT,
        y: t.SupportsFloat | t.Literal[DEFAULT] = DEFAULT,
        angle: t.SupportsFloat | None | t.Literal[DEFAULT] = DEFAULT,
    ) -> Self:
        return self.__class__(
            x if x is not DEFAULT else self.x,
            y if y is not DEFAULT else self.y,
            angle if angle is not DEFAULT else self.angle,
        )

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        x = pop_value(data, float)
        y = pop_value(data, float)
        angle = pop_value(data, float) if data else None
        return cls(x, y, angle)

    # TODO: angle of text is in tenth of degrees

    def __repr__(self) -> str:
        angle = "" if self.angle is None else f", angle={self.angle!r}"
        return f"{self.__class__.__qualname__}({self.x!r}, {self.y!r}{angle})"

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.x
        yield self.y
        yield "angle", self.angle, None

    @property
    def cdr(self) -> t.Iterator[KiValue]:
        yield self.x
        yield self.y
        if self.angle is not None:
            yield self.angle

    def __eq__(self, other: t.Any) -> bool:
        if not isinstance(other, At):
            return False
        return (
            math.isclose(self.x, other.x)
            and math.isclose(self.y, other.y)
            and (
                (self.angle is None and other.angle is None)
                or (
                    self.angle is not None
                    and other.angle is not None
                    and math.isclose(self.angle, other.angle)
                )
            )
        )

    def __add__(self, other: Self) -> Self:
        rotate_by = math.radians(self.angle or 0)
        sin = math.sin(rotate_by)
        cos = math.cos(rotate_by)
        x = other.x * cos - other.y * sin
        y = other.x * sin + other.y * cos

        angle: float | None = None
        if self.angle is not None or other.angle is not None:
            angle = (self.angle or 0) + (other.angle or 0)

        return At(self.x + x, self.y + y, angle)

    def __bool__(self) -> bool:
        return bool(self.x) or bool(self.y) or bool(self.angle)

    def __neg__(self) -> Self:
        return self.__class__(-self.x, -self.y, self.angle)  # TODO: should angle be negated too?

    def __invert__(self) -> Self:
        return self.__class__(self.x, self.y, -self.angle)

    def __sub__(self, other: Self) -> Self:
        return self + (-other)

    def __mul__(self, factor: t.SupportsFloat) -> Self:
        return self.__class__(self.x * float(factor), self.y * float(factor), self.angle)

    def round(self, ndigits: int = 6) -> Self:
        return self.__class__(
            x=round(self.x, ndigits),
            y=round(self.y, ndigits),
            angle=round(self.angle, ndigits) if self.angle is not None else None,
        )

    @property
    def mirror_x(self) -> Self:
        return self.__class__(-self.x, self.y, self.angle)

    @property
    def mirror_y(self) -> Self:
        return self.__class__(self.x, -self.y, self.angle)

    def rotate(self, angle: t.SupportsFloat) -> Self:
        return At(0, 0, angle) + self

    @property
    def unangled(self) -> Self:
        return self if self.angle is None else self.copy(angle=None)

    @property
    def angled(self) -> Self:
        return self if self.angle is not None else self.copy(angle=0)

    @property
    def coords(self) -> tuple[float, float]:
        return (self.x, self.y)


# t.SupportsRound
