from __future__ import annotations

import sys
import typing as t
from abc import ABC, abstractmethod
from enum import EnumMeta

from .._util import Self, extract
from ..format import KiListMixin
from ..list import List
from ..typing import KiValue
from ._helpers import Unlazificatable, check_leftover_data


class Structure(KiListMixin, ABC):
    car: t.Final[str] = t.cast(str, ...)  # type: ignore[reportIncompatibleMethodOverride]
    __forbid_extra_list_data: t.ClassVar[bool] = False
    __slots__ = ("_parent",)
    _parent: Structure | t.Mapping[str, str] | None

    def __init_subclass__(
        cls, *, car: str | None = None, allow_extra_list_data: bool = False, **kwargs: t.Any
    ) -> None:
        if car is not None:
            # Write final classvar - we know what we're doing
            type.__setattr__(cls, "car", sys.intern(car))
        cls.__forbid_extra_list_data = not allow_extra_list_data
        assert isinstance(cls, EnumMeta) or isinstance(
            cls.__dict__.get("__slots__"), tuple
        ), f"{cls} -> {cls.__dict__.get('__slots__')}"
        super().__init_subclass__(**kwargs)

    def __init__(self):
        self._parent = None  # will be set by parent
        for child in self.iter_children():
            child.parent = self

    @abstractmethod
    def copy(self) -> Self:
        raise NotImplementedError()

    @property
    def parent(self) -> Structure | None:
        if isinstance(self._parent, Structure):
            return self._parent
        return None

    @parent.setter
    def parent(self, value: Structure | None):
        assert self._parent is None, f"parent of {self} is {self.parent}, expected None"
        self._parent = value

    @property
    def toplevel(self) -> Structure:
        if isinstance(self._parent, Structure):
            return self._parent.toplevel
        return self

    @property
    def env(self) -> t.Mapping[str, str] | None:
        if isinstance(self._parent, Structure):
            return self._parent.env
        return self._parent

    @env.setter
    def env(self, value: t.Mapping[str, str] | None):
        assert self._parent is None, f"parent of {self} is {self.parent}, expected None"
        self._parent = value

    @abstractmethod
    def iter_children(self) -> t.Iterable[Structure]:
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        raise NotImplementedError

    @classmethod
    def from_list(cls, data_l: List) -> Self:
        if data_l.car is not cls.car:
            raise ValueError(
                f"{cls.__name__}.from_list needs ({cls.car} ...),"
                f" got ({data_l.car} ...) instead"
            )
        data = data_l.cdr
        rv = cls.from_list_data(data)
        check_leftover_data(cls.__qualname__, data, enforce=cls.__forbid_extra_list_data)
        return rv

    @classmethod
    def _is_concrete_structure(cls):
        return "car" in cls.__dict__

    @classmethod
    def __concrete_subclasses(cls) -> t.Iterator[type[Self]]:
        for scls in cls.__subclasses__():
            if scls._is_concrete_structure():
                yield scls
            yield from scls.__concrete_subclasses()

    @classmethod
    def extract_from(cls, data: list[KiValue]) -> t.Iterator[Self]:
        # If it has `car` defined, it's concrete - don't descend into subclasses
        if cls._is_concrete_structure():
            return extract(
                data,
                lambda elt: (
                    cls.from_list(elt) if isinstance(elt, List) and elt.car is cls.car else None
                ),
            )

        # Intermediate class, construct dictionary of subclasses and return
        # list of subclasses
        subs = {scls.car: scls for scls in cls.__concrete_subclasses()}
        return extract(
            data,
            lambda elt: (
                subs[elt.car].from_list(elt) if isinstance(elt, List) and elt.car in subs else None
            ),
        )

    def unlazificate(self) -> None:
        if isinstance(sup := super(), Unlazificatable):
            sup.unlazificate()
