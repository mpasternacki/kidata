from __future__ import annotations

import typing as t

if t.TYPE_CHECKING:
    from typing_extensions import Self

from .._preloadable import PreloadableMapping
from ..typing import KiValue
from .kicad_symbol_lib import KicadSymbolLib
from .lib import Lib
from .parseable_structure import ParseableStructure
from .symbol import FlatSymbol


class SymLibTable(
    ParseableStructure,
    PreloadableMapping[str, FlatSymbol],
    car="sym_lib_table",
    suffix="sym-lib-table",
):
    __slots__ = ("_libs", "lib")
    _libs: dict[str, Lib]
    lib: KicadSymbolLib.Container

    def __init__(self, libs: t.Iterable[Lib]):
        self._libs = {lib.name: lib for lib in libs}
        self.lib = KicadSymbolLib.Container(self._libs.values())  # intentionally no parent
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self._libs.values())

    __copy__ = copy

    def iter_children(self):
        yield from self._libs.values()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        return cls(Lib.extract_from(data))

    @property
    def cdr(self) -> t.Iterable[KiValue]:
        yield from self._libs.values()

    def unlazificate(self):
        self.lib.unlazificate()
        super().unlazificate()

    def preload(self) -> None:
        self.lib.unlazificate1()

    def __getitem__(self, k: str) -> FlatSymbol:
        try:
            (lib_name, sym_name) = k.split(":")
        except ValueError:
            raise KeyError(f"{k!r}: not a lib:sym pair")

        if sym_name == "":
            raise KeyError(f"{k!r}: symbol name empty")

        try:
            lib = self.lib[lib_name]
        except KeyError:
            raise KeyError(f"{k!r}: library {lib_name!r} not found")

        try:
            return lib[sym_name].flatten(lib_name)
        except KeyError:
            raise KeyError(f"{k!r}: symbol {sym_name!r} not found in library {lib_name!r}")

    def __iter__(self) -> t.Iterator[str]:
        for lib_name, lib in self.lib.items():
            for sym_name in lib:
                yield f"{lib_name}:{sym_name}"

    def __len__(self) -> int:
        return sum(len(lib) for lib in self.lib.items())
