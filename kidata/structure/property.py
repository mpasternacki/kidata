from __future__ import annotations

import typing as t

from .._util import Self
from ..api import DEFAULT
from ..typing import KiValue
from ._helpers import extract_value, must, only, pop_value
from .at import At
from .graphical import Effects
from .structure import Structure


@t.final
class Property(Structure, car="property"):
    FIXED_IDS = {
        "Reference": 0,
        "Value": 1,
        "Footprint": 2,
        "Datasheet": 3,
    }
    MIN_USER_ID = max(FIXED_IDS.values()) + 1

    __slots__ = (
        "name",
        "value",
        "id",
        "at",
        "effects",
    )
    name: t.Final[str]
    value: t.Final[str]
    id: t.Final[int]
    at: t.Final[At]
    effects: t.Final[Effects | None]

    def __init__(
        self,
        name: str,
        value: str,
        *,
        id: int,
        at: At | None = None,
        effects: Effects | None | t.Literal[DEFAULT] = DEFAULT,
    ):
        if name in Property.FIXED_IDS:
            assert id == Property.FIXED_IDS[name]
        self.name = name
        self.value = value
        # TODO: ensure fixed ids
        self.id = id
        self.at = at or At(0, 0, 0)
        self.effects = Effects() if effects is DEFAULT else effects
        super().__init__()

    def copy(
        self,
        value: str | None = None,
        id: int | None = None,
        at: At | None = None,
        effects: Effects | None | t.Literal[DEFAULT] = DEFAULT,
    ) -> Self:
        if effects is DEFAULT:
            effects = self.effects.copy() if self.effects else None
        return self.__class__(
            self.name,
            value if value is not None else self.value,
            id=id if id is not None else self.id,
            at=at if at is not None else self.at.copy(),
            effects=effects,
        )

    __copy__ = copy

    def hidden(self, hide: bool = True) -> Self:
        return self.copy(
            effects=Effects(hide=hide) if self.effects is None else self.effects.copy(hide=hide)
        )

    def iter_children(self):
        yield self.at
        if self.effects:
            yield self.effects

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        name = pop_value(data, str)
        value = pop_value(data, str)
        id_ = must(extract_value(data, "id", int))
        (at,) = At.extract_from(data)
        effects = only(Effects.extract_from(data))

        return cls(name, value, id=id_, at=at, effects=effects)

    @property
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...]]:
        yield self.name
        yield self.value
        yield "id", self.id
        yield self.at.angled
        if self.effects:
            yield self.effects

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.name
        yield self.value
        yield "id", self.id
        if self.at:
            yield "at", self.at
        yield "effects", self.effects
