from __future__ import annotations

import typing as t

from .._util import Self
from ..bareword import Bareword
from ..typing import KiValue
from ._helpers import pop_value
from .structure import Structure


@t.final
class Paper(Structure, car="paper"):
    DEFAULT_SIZE: t.Final = "A4"

    __slots__ = ("size", "orientation")
    size: t.Final[str]
    orientation: t.Final[Bareword | None]

    def __init__(self, size: str, *, orientation: str | Bareword | None = None):
        if isinstance(orientation, str):
            orientation = Bareword(orientation)
        self.size = size
        self.orientation = orientation
        super().__init__()

    def copy(self) -> Self:
        return self.__class__(self.size, orientation=self.orientation)

    __copy__ = copy

    def iter_children(self):
        return ()

    @classmethod
    def from_list_data(cls, data: list[KiValue]) -> Self:
        size = pop_value(data, str)
        orientation = pop_value(data, Bareword) if data else None
        return cls(size, orientation=orientation)

    @property
    def cdr(self) -> t.Iterator[KiValue]:
        yield self.size
        if self.orientation:
            yield self.orientation
