from __future__ import annotations

from abc import abstractmethod
from collections import ChainMap
from collections.abc import Mapping
from typing import TYPE_CHECKING, Generic, TypeVar

KT = TypeVar("KT", covariant=True)
VT = TypeVar("VT", covariant=True)


class PreloadableMapping(Mapping[KT, VT]):
    __slots__ = ()

    @abstractmethod
    def preload(self) -> None:
        ...


class PreloadableChainMap(ChainMap[KT, VT], PreloadableMapping[KT, VT], Generic[KT, VT]):
    maps: list[PreloadableMapping[KT, VT]]  # type: ignore[reportIncompatibleVariableOverride]

    if TYPE_CHECKING:

        def __init__(self, *args: Mapping[KT, VT]):
            ...  # override argument type MutableMapping -> Mapping

    def preload(self):
        for map in self.maps:
            map.preload()
