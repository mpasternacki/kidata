import typing as t


class KiFormattable(t.Protocol):
    def _kiformat_(self, f: t.TextIO) -> None:
        ...


KiValue = KiFormattable | float | int | str | bool
