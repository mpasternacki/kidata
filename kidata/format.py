import abc
import io
import typing as t

import ujson

from .typing import KiValue

__all__ = ["kiformat", "KiListMixin"]


def kiformat1(val: KiValue, f: t.TextIO) -> None:
    match val:
        case True:
            f.write("yes")
        case False:
            f.write("no")
        case kf if hasattr(kf, "_kiformat_"):
            # the above is (fast) type guard for KiFormattable, and in
            # this place even t.cast() has some performance overhead, so
            # just ignore the typing "problem"
            kf._kiformat_(f)  # type: ignore
        case int():
            f.write(str(val))  # faster than ujson
        case float():
            if val.is_integer():
                f.write(str(int(val)))
            else:
                f.write(ujson.dumps(val))
        case str():
            f.write(ujson.dumps(val, escape_forward_slashes=False, ensure_ascii=False))
        case _:
            raise ValueError(f"Don't know how to format {val!r} for KiCad")


def kiformat_list(
    car: str,
    cdr: t.Iterable[KiValue | tuple[KiValue, ...] | None],
    f: t.TextIO,
    *,
    separator: str = "\n",
) -> None:
    f.write("(")
    f.write(car)
    for v in cdr:
        if v is None:
            continue
        f.write(separator)
        if v.__class__ is tuple:
            vi = iter(v)  # type: ignore # it's a tuple
            vcar = next(vi)
            assert isinstance(vcar, str)
            kiformat_list(vcar, vi, f)
        else:
            kiformat1(v, f)  # type: ignore # all that's left is a KiValue
    f.write(")")


class KiListMixin(abc.ABC):
    KIFORMAT_ITEM_SEPARATOR: str = "\n"
    __slots__ = ()

    @property
    @abc.abstractmethod
    def car(self) -> str:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def cdr(self) -> t.Iterable[KiValue | tuple[KiValue, ...] | None]:
        raise NotImplementedError

    def _kiformat_(self, f: t.TextIO) -> None:
        kiformat_list(self.car, self.cdr, f, separator=self.__class__.KIFORMAT_ITEM_SEPARATOR)

    def __rich_repr__(self) -> t.Iterator[t.Any]:
        yield self.car
        for v in self.cdr:
            yield None, v


@t.overload
def kiformat(val: KiValue, output: t.TextIO) -> None:
    ...  # pragma: no cover


@t.overload
def kiformat(val: KiValue) -> str:
    ...  # pragma: no cover


def kiformat(val: KiValue, output: t.TextIO | None = None) -> str | None:
    if output:
        kiformat1(val, output)
        return None

    buf = io.StringIO()
    kiformat1(val, buf)
    return buf.getvalue()
