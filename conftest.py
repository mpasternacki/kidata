import os
from pathlib import Path
from warnings import warn

import pytest
from _pytest.python import Metafunc

import kidata.env


class KiCadTestEnv(kidata.env.KiCadEnv):
    CACHE_BASE = Path(__file__).parent / ".cache/git"
    CACHE_PATHS = {
        "KICAD6_DEMO_DIR": CACHE_BASE / "kicad/demos",
        "KICAD6_FOOTPRINT_DIR": CACHE_BASE / "kicad-footprints",
        "KICAD6_SYMBOL_DIR": CACHE_BASE / "kicad-symbols",
    }

    def __init__(self, **kwargs: str):
        for name, path in self.CACHE_PATHS.items():
            if path.exists():
                if name in os.environ and os.environ[name] != str(path):
                    warn(
                        f"Ignoring {name}={os.environ[name]!r} in environment"
                        f" because {path} cache exists"
                    )
                kwargs[name] = str(path)
        super().__init__(**kwargs)

    @property
    def global_sym_lib_table_path(self) -> Path:
        if (symbol_dir := self.CACHE_PATHS["KICAD6_SYMBOL_DIR"]).exists():
            return symbol_dir / "sym-lib-table"
        return super().global_sym_lib_table_path

    @property
    def global_fp_lib_table_path(self) -> Path:
        if (footprint_dir := self.CACHE_PATHS["KICAD6_FOOTPRINT_DIR"]).exists():
            return footprint_dir / "fp-lib-table"
        return super().global_fp_lib_table_path


ENV = KiCadTestEnv()


@pytest.fixture
def kicad_env() -> kidata.env.KiCadEnv:
    return ENV.copy()


def pytest_generate_tests(metafunc: Metafunc) -> None:
    def gen_path_glob(fixture_name: str, dir_name: str, glob: str) -> None:
        if fixture_name in metafunc.fixturenames:
            if (data_dir := Path(ENV[dir_name])).exists():
                values = tuple(data_dir.glob(glob))
                metafunc.parametrize(
                    fixture_name,
                    values,
                    ids=(str(v.relative_to(data_dir)) for v in values),
                )
            else:
                # skip - can we do it better?
                metafunc.parametrize(fixture_name, (), ids=(f"{dir_name} not found",))

    gen_path_glob("kicad6_demo_sch_path", "KICAD6_DEMO_DIR", "**/*.kicad_sch")
    gen_path_glob("kicad6_demo_pcb_path", "KICAD6_DEMO_DIR", "**/*.kicad_pcb")
    gen_path_glob("kicad6_lib_footprint_path", "KICAD6_FOOTPRINT_DIR", "*.pretty/*.kicad_mod")
    gen_path_glob("kicad6_lib_symbol_path", "KICAD6_SYMBOL_DIR", "*.kicad_sym")
