from __future__ import annotations

from pathlib import Path

import pytest

from kidata import kiformat, kiparse


@pytest.mark.slow
def test_parse_symbols(kicad6_lib_symbol_path: Path) -> None:
    (result,) = kiparse(kicad6_lib_symbol_path.read_text())
    kiformat(result)

    # TODO: serialize, reparse, compare results
    # TODO: or compare original with serialized, ignoring spaces? But order might be different


@pytest.mark.slow
def test_parse_footprints(kicad6_lib_footprint_path: Path) -> None:
    (result,) = kiparse(kicad6_lib_footprint_path.read_text())
    kiformat(result)


@pytest.mark.slow
def test_parse_sch(kicad6_demo_sch_path: Path) -> None:
    (result,) = kiparse(kicad6_demo_sch_path.read_text())
    kiformat(result)


# TODO: other files in demos
