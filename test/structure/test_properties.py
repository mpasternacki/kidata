from kidata.env import KiCadEnv


def test_library_symbol_property(kicad_env: KiCadEnv):
    sym = kicad_env.global_sym_lib_table.lib["4xxx"]["4066"]

    parent = sym.extends
    assert parent is not None

    assert sym.name == "4066"
    assert parent.name == "4016"

    assert sym.properties["Value"] == "4066"
    assert sym.properties["Reference"] == "U"
    assert parent.properties["Value"] == "4016"
    assert parent.properties["Reference"] == "U"

    flat = sym.flatten()
    assert flat.properties["Value"] == "4066"
    assert flat.properties["Reference"] == "U"

    flat.properties["Reference"] = "U0"
    assert flat.properties["Reference"] == "U0"
    assert sym.properties["Reference"] == "U"
    assert parent.properties["Reference"] == "U"
