from __future__ import annotations

from pathlib import Path

import pytest

from kidata import kiformat
from kidata.structure.kicad_pcb import KicadPcb


@pytest.mark.slow
@pytest.mark.filterwarnings("ignore:Extra data present. (Pad|Footprint). ")  # FIXME?
def test_parse_kicad_pcb(kicad6_demo_pcb_path: Path) -> None:
    if str(kicad6_demo_pcb_path).endswith("microwave/microwave.kicad_pcb"):
        pytest.skip("Skipping microwave/microwave.kicad_pcb")
    result = KicadPcb.from_file(kicad6_demo_pcb_path)
    result.unlazificate()
    # TODO: test something
    kiformat(result)
