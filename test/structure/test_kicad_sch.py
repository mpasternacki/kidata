from __future__ import annotations

from pathlib import Path

import pytest

from kidata import kiformat
from kidata.structure.kicad_sch import KicadSch


@pytest.mark.slow
@pytest.mark.filterwarnings(
    "ignore:Extra data present. only. <kidata\\.structure\\.graphical\\.\\w+\\.Stroke object"
)
def test_parse_kicad_sch(kicad6_demo_sch_path: Path) -> None:
    if str(kicad6_demo_sch_path).endswith("electric/electric.kicad_sch"):
        pytest.skip("Skipping electric/electric.kicad_sch")
    result = KicadSch.from_file(kicad6_demo_sch_path)
    result.unlazificate()
    # TODO: test something
    kiformat(result)
