from kidata.bareword import Bareword
from kidata.format import kiformat
from kidata.list import List
from kidata.structure._helpers import BarewordEnum


class _ExampleEnum(BarewordEnum):
    __slots__ = ()
    FOO = "foo"
    BAR = "bar"


def test_bareword_enum_kiformat():
    assert kiformat(_ExampleEnum.FOO) == "foo"
    assert (
        kiformat(List("t") << _ExampleEnum.FOO << _ExampleEnum.BAR << "baz") == '(t foo bar "baz")'
    )


def test_bareword_enum_extract_from_not_present():
    data = [1, 2, "foo"]
    result = tuple(_ExampleEnum.extract_from(data))

    assert result == ()
    assert data == [1, 2, "foo"]


def test_bareword_enum_extract_from_present():
    data = [1, 2, Bareword("foo"), 3, Bareword("bar"), "bar", Bareword("foo")]
    result = tuple(_ExampleEnum.extract_from(data))

    assert result == (_ExampleEnum.FOO, _ExampleEnum.BAR, _ExampleEnum.FOO)
    assert data == [1, 2, 3, "bar"]
