import typing as t

import pytest

import kidata.structure  # noqa:W0611,F401 # for side effects
from kidata.structure.structure import Structure


def _gen_recursive_subclasses(cls: type) -> t.Iterator[type]:
    yield cls
    for scls in cls.__subclasses__():
        yield from _gen_recursive_subclasses(scls)


def _qualname(o: t.Any) -> str:
    if isinstance(o, type):
        return o.__qualname__
    raise ValueError(o)


@pytest.mark.parametrize(
    "concrete_structure_class",
    sorted(
        set(
            cls
            for cls in _gen_recursive_subclasses(Structure)
            if "car" in cls.__dict__ and cls is not Structure
        ),
        key=_qualname,
    ),
)
def test_concrete_structures(
    concrete_structure_class: type[Structure],
) -> None:
    for cls in concrete_structure_class.mro():
        if cls is not object:
            # no better idea how to ensure all classes are slotted without instantiating them
            assert (
                "__slots__" in cls.__dict__
            ), f"Superclass {cls} of {concrete_structure_class} doesn't have __slots__"
    assert not getattr(concrete_structure_class, "__abstractmethods__", None)
