from __future__ import annotations

from pathlib import Path

import pytest

from kidata import kiformat
from kidata.structure.kicad_symbol_lib import KicadSymbolLib


@pytest.mark.slow
@pytest.mark.filterwarnings("ignore:Duplicate pin number")  # TODO
def test_parse_symbols(kicad6_lib_symbol_path: Path) -> None:
    result = KicadSymbolLib.from_file(kicad6_lib_symbol_path)
    result.unlazificate()
    # TODO: test something
    kiformat(result)
