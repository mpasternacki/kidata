import math

import hypothesis as ht
import hypothesis.strategies as hs

from kidata.structure.at import At

actual_floats = hs.floats(allow_nan=False, allow_infinity=False)


@ht.given(
    x=actual_floats,
    y=actual_floats,
    angle=actual_floats,
    not_y=actual_floats,
    not_angle=actual_floats,
)
def test_at_accessors_with_angle(
    x: float, y: float, angle: float, not_y: float, not_angle: float
) -> None:
    ht.assume(not math.isclose(not_y, y))
    ht.assume(not math.isclose(not_angle % 360, angle % 360))

    at = At(x, y, angle)
    assert at.x == x
    assert at.y == y

    assert at.angle is not None
    assert math.fabs(at.angle) <= 360
    assert math.isclose(at.angle, angle % 360)

    assert at == At(x, y, angle)
    assert at != At(x, not_y, angle)
    assert at != At(x, y, not_angle)
    assert at != At(x, y)


def test_add() -> None:
    assert At(1, 2) + At(3, 4) == At(4, 6)
    assert At(1, 2) + At(3, 4, 0) == At(4, 6, 0)
    assert At(1, 2) + At(3, 4, 10) == At(4, 6, 10)
    assert At(1, 2, 180) + At(3, 4, 10) == At(-2, -2, 190)
    assert At(1, 2, 180) + At(3, 4, 0) == At(-2, -2, 180)
    assert At(1, 2, 180) + At(3, 4) == At(-2, -2, 180)

    # TODO: more cases for rotation
